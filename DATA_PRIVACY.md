[German] Datenschutzerklaerung der App FaMI Quiz
=========================================
Juli 2019
-----------------------------------------

# Anbieter/Betreiber der App
Diese App wurde entwickelt von der Klasse IT2B/16 mit freundlicher Unterstuetzung des Joseph DuMont-Berufskollegs.

# Kontaktdaten der App
Wenn Sie weitere Fragen zum Datenschutz haben, wenden Sie sich an die unten stehende Kontaktadresse.
```
Jan W. & Annika Gassner
Gladbacher Strasse 86
50189 Elsdorf
E-Mail-Adresse: it2b.famiapp@gmail.com
```

# Zugriff der App
Die Android-App benoetigt Zugriff auf die Netzwerkverbindungen des Telefons. Dies ist noetig, um die Inhalte der App laden und aktualisieren zu koennen. Darueber hinaus benoetigt die App Zugriff auf die Speicherkarte des Telefons. Dies ist noetig, weil der Nutzer der App die Moeglichkeit hat, Inhalte vom Telefon auf die Speicherkarte zu verschieben um so Speicherplatz zu sparen.




[English] Privacy policy of the App FaMI Quiz
=========================================
July 2019
-----------------------------------------

# Provider/operator of the app
This app was developed by the IT2B/16 class with the technical support of the Joseph DuMont Berufskolleg.

# Contact details
If you have any further questions regarding data privacy, please contact us at the address below.
```
Jan W. & Annika Gassner
Gladbacher Strasse 86
50189 Elsdorf, Germany
E-Mail: it2b.famiapp@gmail.com
```

# Required app permissions
The Android app requires access to the phone's network connections. This is necessary for downloading and updating the contents of the app. The app also needs access to the phone's memory card. This is necessary because the user of the app can move content from the phone to the memory card to save storage space.