﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using FaMI_Quiz.Provider;
using FaMI_Quiz.View;
using Xamarin.Forms;

namespace FaMI_Quiz.ViewModel {
    public class QuizVM : INotifyPropertyChanged {
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            if(PropertyChanged == null) return;
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private ObservableCollection<QuestionVM> questions;
        private int maxPoints = -1;

        public QuizVM() { }
        public QuizVM(List<MainCategoryVM> mainCategories, bool? questionState, int count, bool load_from_database) {
            if(load_from_database) questions = QuestionProvider.LoadRandomQuestionsWithAnswersFromDB(mainCategories, questionState, count);
            else questions = QuestionProvider.LoadRandomQuestionsWithAnswers(mainCategories, questionState, count);
            CreateIndexSetType();
        }
        public QuizVM(List<CategoryVM> categories, bool? questionState, int count, bool load_from_database) {
            if(load_from_database) questions = QuestionProvider.LoadRandomQuestionsWithAnswersFromDB(categories, questionState, count);
            else questions = QuestionProvider.LoadRandomQuestionsWithAnswers(categories, questionState, count);
            CreateIndexSetType();
        }

        public ObservableCollection<QuestionVM> GetQuestions { get { return questions; } }
        public int GetQuestionCount { get { if(questions != null) return questions.Count; else return -1; } }
        public int GetFirstUnconfirmedIndex {
            get {
                for(int i = 0; i < questions.Count; ++i) {
                    if(!questions[i].AreAnswersConfirmed) return i;
                }
                return -1;
            }
        }
        public int GetUnansweredCount {
            get {
                int count = 0;
                for(int i = 0; i < questions.Count; ++i) if(!questions[i].AreAnswersConfirmed) ++count;
                return count;
            }
        }
        public int GetCorrectlyAnsweredCount {
            get {
                int count = 0;
                for(int i = 0; i < questions.Count; ++i) if(questions[i].GotCorrectlyAnswered) ++count;
                return count;
            }
        }
        public double GetQuestionPercent {
            get {
                return (double)((double)GetCorrectlyAnsweredCount / (double)questions.Count) * 100;
            }
        }
        public double GetPointsPercent {
            get {
                int gotPoints = 0;
                int maxPoints_tmp = 0;
                foreach(QuestionVM qVM in questions) {
                    maxPoints_tmp += qVM.Points;
                    if(qVM.GotCorrectlyAnswered) gotPoints += qVM.Points;
                }
                if(maxPoints == -1) maxPoints = maxPoints_tmp;
                return (double)((double)gotPoints / (double)maxPoints) * 100;
            }
        }
        public int GetPointsGotCount {
            get {
                int gotPoints = 0;
                foreach(QuestionVM qVM in questions) if(qVM.GotCorrectlyAnswered) gotPoints += qVM.Points;
                return gotPoints;
            }
        }
        public int GetPointsCount {
            get {
                if(maxPoints == -1) {
                    maxPoints = 0;
                    foreach(QuestionVM qVM in questions) maxPoints += qVM.Points;
                }
                return maxPoints;
            }
        }
        public string GetGradeResult {
            get {
                Dictionary<double, string> grades = new Dictionary<double, string> {
                    { 100,"sehr gut" },
                    { 92,"sehr gut" },
                    { 81,"gut" },
                    { 70,"befriedigend" },
                    { 59,"ausreichend" },
                    { 48,"mangelhaft" },
                    { 0,"ungenügend" }
                };
                KeyValuePair<double, string> gradeTemp = new KeyValuePair<double, string>(-1, string.Empty);
                double percentage = GetQuestionPercent;
                foreach(KeyValuePair<double, string> grade in grades) {
                    if(gradeTemp.Key == -1 && percentage >= grade.Key) gradeTemp = grade; //erster Lauf, bzw. Treffer
                    else if(gradeTemp.Key != -1 && percentage >= grade.Key && gradeTemp.Key < grade.Key) gradeTemp = grade;
                }
                return gradeTemp.Value;
            }
        }
        public string GetGradeResultPoints {
            get {
                Dictionary<double, string> grades = new Dictionary<double, string> {
                    { 100,"sehr gut" },
                    { 92,"sehr gut" },
                    { 81,"gut" },
                    { 70,"befriedigend" },
                    { 59,"ausreichend" },
                    { 48,"mangelhaft" },
                    { 0,"ungenügend" }
                };
                KeyValuePair<double, string> gradeTemp = new KeyValuePair<double, string>(-1, string.Empty);
                double percentage = GetPointsPercent;
                foreach(KeyValuePair<double, string> grade in grades) {
                    if(gradeTemp.Key == -1 && percentage >= grade.Key) gradeTemp = grade; //erster Lauf, bzw. Treffer
                    else if(gradeTemp.Key != -1 && percentage >= grade.Key && gradeTemp.Key < grade.Key) gradeTemp = grade;
                }
                return gradeTemp.Value;
            }
        }
        public string GetQuestionResult {
            get {
                return GetCorrectlyAnsweredCount + " von " + questions.Count + " Fragen richtig";
            }
        }
        public string GetPointResult {
            get {
                return GetPointsGotCount + " von " + GetPointsCount + " Punkten erreicht";
            }
        }
        private void CreateIndexSetType() {
            for(int i = 0; i < questions.Count; ++i) {
                questions[i].Index = (i + 1);
                questions[i].TargetType = typeof(QuizMDPageDetail);
            }
        }
        public void SaveResults(Page p) {
            foreach(QuestionVM qVM in questions) qVM.Save(p);
        }
        public void ResetAnswers() {
            foreach(QuestionVM qVM in questions) {
                qVM.Reset();
                foreach(AnswerVM aVM in qVM.Answers) aVM.Reset();
            }
        }
    }
}
