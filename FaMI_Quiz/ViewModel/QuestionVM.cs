﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace FaMI_Quiz.ViewModel
{
    public class QuestionVM : INotifyPropertyChanged {
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            if(PropertyChanged == null) return;
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private Question question;
        private bool correctlyAnswered = false;
        private bool answersConfirmed = false;
        private int index = -1;

        public Question Question {
            get { return question; }
        }

        public int ID {
            get { return question.q_id; }
        }
        public string Text {
            get { return question.q_text; }
        }
        public int Points {
            get { return question.q_points; }
        }
        public bool IsMultipleChoice {
            get { if(question.q_is_multiple_choice == 1) return true; else return false; }
        }
        public bool? IsAnswered {
            get { if(question.q_is_answered == 1) return true; else if(question.q_is_answered == -1) return false; else return null; }
            set { if(value == true) question.q_is_answered = 1; else if(value == false) question.q_is_answered = -1; else question.q_is_answered = 0; }
        }
        public int CategoryID {
            get { return question.q_category_id; }
        }
        public int Index { get { return index; } set { index = value; } }
        public Type TargetType { get; set; }
        public bool GotAnswered {
            get {
                int answerCount = 0;
                int correctAnswerCount = 0;
                foreach(AnswerVM aVM in Answers) {
                    if(aVM.GotTicked) ++answerCount;
                    if(aVM.IsCorrect) ++correctAnswerCount;
                }
                if(!IsMultipleChoice && answerCount == 1) return true; // Einzelfrage braucht eine Antwort
                else if(IsMultipleChoice && ConstVars.Setting_MultipleChoice_DisplayCount && answerCount == correctAnswerCount) return true; //Mehrfachauswahl mit Anzeige der benoetigten Anzahl muss exakt beantwortet werden
                else if(IsMultipleChoice && !ConstVars.Setting_MultipleChoice_DisplayCount && answerCount != 0) return true; //Mehrfachauswahl ohne Anzeige der benoetigten Anzahl kann beliebig beantwortet werden (ergo hoeherer Schwierigkeitsgrad)
                else return false;
            }
        }
        public bool GotCorrectlyAnswered { get { return correctlyAnswered; } }
        public bool AreAnswersConfirmed { get { return answersConfirmed; } }

        public QuestionVM() { }
        public QuestionVM(Question question) {
            this.question = question;
        }
        public ObservableCollection<AnswerVM> Answers { get; set; }

        public int GetNeededAnswerCount {
            get {
                if(!IsMultipleChoice) return 1;
                else {
                    int count = 0;
                    foreach(AnswerVM aVM in Answers) if(aVM.IsCorrect) ++count;
                    return count;
                }
            }
        }

        public int GetAnswerCount {
            get {
                int count = 0;
                foreach(AnswerVM aVM in Answers) if(aVM.GotTicked) ++count;
                return count;
            }
        }

        public bool VerifyAnswersAndConfirm() {
            if(!GotAnswered) return false;
            int correctlyAnsweredCount = 0;
            int correctAnswerCount = 0;
            foreach(AnswerVM aVM in Answers) {
                if(IsMultipleChoice) {
                    if(aVM.IsCorrect) {
                        ++correctAnswerCount;
                        if(aVM.GotTicked) ++correctlyAnsweredCount;
                    }
                } else if(aVM.GotTicked && aVM.IsCorrect) {
                    correctlyAnswered = true;
                    break;
                }
            }
            if(IsMultipleChoice && correctlyAnsweredCount == correctAnswerCount) correctlyAnswered = true;
            answersConfirmed = true;
            OnPropertyChanged("GetBoxStyle");
            return correctlyAnswered;
        }

        public bool Save(Page p) {
            try {
                if(question.q_is_answered != 1 && correctlyAnswered) {
                    question.q_is_answered = 1;
                    App.DBAccess.UpdateQuestion(question);
                    return true;
                } else if(question.q_is_answered == 0 && !correctlyAnswered) {
                    question.q_is_answered = -1;
                    App.DBAccess.UpdateQuestion(question);
                    return true;
                } else return true;
            } catch(Exception ex) {
                p.DisplayAlert("Aktualisieren der Frage fehlgeschlagen", ex.Message, "OK");
                return false;
            }
        }

        public Style GetBoxStyle {
            get {
                Style s = (Style)App.Current.Resources["BoxViewTransparent"];
                if(AreAnswersConfirmed) {
                    if(correctlyAnswered) s = (Style)App.Current.Resources["BoxViewCorrect"];
                    else s = (Style)App.Current.Resources["BoxViewFalse"];
                }
                return s;
            }
        }

        public void Reset() {
            answersConfirmed = false;
            correctlyAnswered = false;
            index = -1;
        }
    }
}
