﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FaMI_Quiz.ViewModel {
    public class AnswerVM : INotifyPropertyChanged {
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            if(PropertyChanged == null) return;
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private Answer answer;
        private bool gotTicked = false;

        public Answer Answer {
            get { return answer; }
        }

        public int ID {
            get { return answer.a_id; }
        }
        public string Text {
            get { return answer.a_text; }
        }
        public bool IsCorrect {
            get { if(answer.a_is_correct == 1) return true; else return false; }
        }
        public byte[] Image {
            get { return answer.a_image; }
            set { answer.a_image = value; }
        }
        public int QuestionID {
            get { return answer.a_question_id; }
        }
        public bool GotTicked { get { return gotTicked; } set { gotTicked = value; } }

        public AnswerVM() { }
        public AnswerVM(Answer script) {
            this.answer = script;
        }

        public void Reset() {
            gotTicked = false;
        }
    }
}
