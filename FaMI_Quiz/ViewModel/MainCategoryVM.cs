﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using FaMI_Quiz.Provider;

namespace FaMI_Quiz.ViewModel
{
    public class MainCategoryVM : INotifyPropertyChanged {
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            if(PropertyChanged == null) return;
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private MainCategory mainCategory;
        private bool gotTicked = false;
        private double progress = -1;
        private int answered = -1;

        public MainCategory MainCategory {
            get { return mainCategory; }
        }


        public int ID {
            get { return mainCategory.mc_id; }
        }
        public string Name {
            get { return mainCategory.mc_name; }
        }
        public string Description {
            get { return mainCategory.mc_description; }
        }
        public bool GotTicked { get { return gotTicked; } set { gotTicked = value; OnPropertyChanged("GotTicked"); } }
        public double GetProgressPercent { get {
                if(progress == -1) CalculateProgress();
                return progress;
        } }
        public int GetProgressCount { get {
                if(answered == -1) CalculateProgress();
                return answered;
        } }

        public MainCategoryVM() { }
        public MainCategoryVM(MainCategory mainCategory) {
            this.mainCategory = mainCategory;
        }
        public ObservableCollection<CategoryVM> Categories { get; set; }

        public void CalculateProgress() {
            progress = 0;
            answered = 0;
            int count = 0;
            if(Categories == null) Categories = CategoryProvider.LoadCategories(this, false);
            foreach(CategoryVM cVM in Categories) {
                if(cVM.Questions == null) cVM.Questions = QuestionProvider.LoadQuestions(cVM, false);
                foreach(QuestionVM qVM in cVM.Questions) {
                    ++count;
                    if(qVM.IsAnswered == true) ++answered;
                }
            }
            if(count == 0) progress = 100;
            else progress = ((double)answered / (double)count) * 100;
        }

        public void Recalculate_and_Notify() {
            CalculateProgress();
            OnPropertyChanged("GetProgressPercent");
            OnPropertyChanged("GetProgressCount");
        }

        public void ResetProgress() {
            App.DBAccess.ResetProgress(this);
            foreach(CategoryVM cVM in Categories) {
                foreach(QuestionVM qVM in cVM.Questions) qVM.IsAnswered = null;
            }
            VML.Notify();
        }
    }
}
