﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FaMI_Quiz.ViewModel
{
    public class ScriptVM : INotifyPropertyChanged {
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            if(PropertyChanged == null) return;
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private Script script;

        public Script Script {
            get { return script; }
        }

        public int ID {
            get { return script.s_id; }
        }
        public string Name {
            get { return script.s_name; }
        }
        public string Description {
            get { return script.s_description; }
        }
        public int Order {
            get { return script.s_order; }
        }
        public byte[] PDF {
            get { return script.s_pdf; }
            set { script.s_pdf = value; }
        }
        public int CategoryID {
            get { return script.s_category_id; }
        }
        public string PDF_Name {
            get { return script.s_pdf_name; }
        }

        public ScriptVM() { }
        public ScriptVM(Script script) {
            this.script = script;
        }
    }
}
