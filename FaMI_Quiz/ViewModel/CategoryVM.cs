﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using FaMI_Quiz.Provider;

namespace FaMI_Quiz.ViewModel
{
    public class CategoryVM : INotifyPropertyChanged {
        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        void OnPropertyChanged([CallerMemberName] string propertyName = "") {
            if(PropertyChanged == null) return;
            PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private Category category;
        private bool gotTicked = false;
        private double progress = -1;
        private int answered = -1;
        private int progressPointsNumber = -1;
        private int maxPoints = -1;

        public Category Category {
            get { return category; }
        }

        public int ID {
            get { return category.c_id; }
        }
        public string Name {
            get { return category.c_name; }
        }
        public string Description {
            get { return category.c_description; }
        }
        public int MainCategoryID {
            get { return category.c_maincategory_id; }
        }
        public bool GotTicked { get { return gotTicked; } set { gotTicked = value; OnPropertyChanged("GotTicked"); } }
        public double GetProgressPercent {
            get {
                if(progress == -1) CalculateProgress();
                return progress;
            }
        }
        public int GetProgressCount {
            get {
                if(answered == -1) CalculateProgress();
                return answered;
            }
        }
        public int GetProgressPoints {
            get {
                if(progressPointsNumber == -1) CalculateProgress();
                return progressPointsNumber;
            }
        }
        public int GetPointsCount {
            get {
                if(maxPoints == -1) CalculateProgress();
                return maxPoints;
            }
        }

        public CategoryVM() { }
        public CategoryVM(Category category) {
            this.category = category;
        }
        public ObservableCollection<ScriptVM> Scripts { get; set; }
        public ObservableCollection<QuestionVM> Questions { get; set; }

        public void CalculateProgress() {
            progress = 0;
            answered = 0;
            progressPointsNumber = 0;
            maxPoints = 0;
            int count = 0;
            if(Questions == null) Questions = QuestionProvider.LoadQuestions(this, false);
            foreach(QuestionVM qVM in Questions) {
                ++count;
                maxPoints += qVM.Points;
                if(qVM.IsAnswered == true) {
                    ++answered;
                    progressPointsNumber += qVM.Points;
                }
            }
            if(count == 0) progress = 100;
            else progress = ((double)answered / (double)count) * 100;
        }

        public int GetQuestionCount { get { return Questions.Count;  } }
        public int GetUnansweredQuestionCount {
            get {
                int count = 0;
                foreach(QuestionVM qVM in Questions) if(qVM.IsAnswered == null) ++count;
                return count;
            }
        }
        public int GetIncorrectAnsweredQuestionCount {
            get {
                int count = 0;
                foreach(QuestionVM qVM in Questions) if(qVM.IsAnswered == false) ++count;
                return count;
            }
        }
        public string GetPointsStatistic {
            get {
                return GetProgressPoints + " von " + GetPointsCount + " Punkten erreicht";
            }
        }
        public string GetQuestionStatistic {
            get {
                return GetProgressCount + " von " + GetQuestionCount + " Fragen richtig";
            }
        }

        public void Recalculate_and_Notify() {
            CalculateProgress();
            OnPropertyChanged("GetProgressPercent");
            OnPropertyChanged("GetQuestionStatistic");
            OnPropertyChanged("GetPointsStatistic");
        }

        public void ResetProgress() {
            App.DBAccess.ResetProgress(this);
            foreach(QuestionVM qVM in Questions) qVM.IsAnswered = null;
            VML.Notify();
        }
    }
}
