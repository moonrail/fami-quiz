﻿using Xamarin.Forms;

namespace FaMI_Quiz {
    public static class ThemeSwitcher {
        public static void Switch(bool firstRun) {
            if(ConstVars.Setting_DarkTheme) Dark();
            else if(!firstRun) Light(); //firstRun verhindert komplettes Neuzeichnen nach Start wenn eh schon Light aktiviert ist
        }

        //Deklaration/Initialisierung in App.xaml
        //um ListView-SelectedItemColor zu aendern HEX-Wert in Android/Ressources/Drawable/ViewCellBackground.xml anpassen

        private static void Dark() {
            if(Device.RuntimePlatform == Device.Android) {
                Application.Current.Resources["veryBigFont"] = 24.0;
                Application.Current.Resources["bigFont"] = 20.0;
                Application.Current.Resources["mediumFont"] = 16.0;
                Application.Current.Resources["smallFont"] = 14.0;
                Application.Current.Resources["iconSelectAll"] = "@drawable/ic_select_all_white_48dp.png";
                Application.Current.Resources["iconRefresh"] = "@drawable/ic_refresh_white_48dp.png";
                Application.Current.Resources["iconPlayArrow"] = "@drawable/ic_play_arrow_white_48dp.png";
                Application.Current.Resources["iconMenu"] = "@drawable/ic_menu_white_48dp.png";
                Application.Current.Resources["imageAppBG"] = "@drawable/app_bg_dark.png";
            } else if(Device.RuntimePlatform == Device.UWP) {
                Application.Current.Resources["veryBigFont"] = 32.0;
                Application.Current.Resources["bigFont"] = 24.0;
                Application.Current.Resources["mediumFont"] = 18.0;
                Application.Current.Resources["smallFont"] = 16.0;
                //Application.Current.Resources["iconSelectAll"] = "";
                //Application.Current.Resources["iconRefresh"] = "";
                //Application.Current.Resources["iconPlayArrow"] = "";
                //Application.Current.Resources["iconMenu"] = "";
            } else { //iOS
                Application.Current.Resources["veryBigFont"] = 24.0;
                Application.Current.Resources["bigFont"] = 20.0;
                Application.Current.Resources["mediumFont"] = 16.0;
                Application.Current.Resources["smallFont"] = 14.0;
                Application.Current.Resources["iconSelectAll"] = "ic_select_all_white_48pt.png";
                Application.Current.Resources["iconRefresh"] = "ic_refresh_white_48pt.png";
                Application.Current.Resources["iconPlayArrow"] = "ic_play_arrow_white_48pt.png";
                Application.Current.Resources["iconMenu"] = "ic_menu_white_48pt.png";
            }
            Application.Current.Resources["backgroundColor"] = Color.FromHex("#000000");
            Application.Current.Resources["barBackgroundColor"] = Color.FromHex("356DA5");
            Application.Current.Resources["barTextColor"] = Color.FromHex("#DDDDDD");
            Application.Current.Resources["seperatorColor"] = Color.FromHex("#BCBCBC");
            Application.Current.Resources["textColorHeader"] = Color.FromHex("#FFFFFF");
            Application.Current.Resources["textColorTitle"] = Color.FromHex("#4082C4");//2699FF
            Application.Current.Resources["textColor"] = Color.FromHex("#DDDDDD");
            Application.Current.Resources["headerColor"] = Color.FromHex("#356DA5");
            Application.Current.Resources["colorCorrect"] = Color.FromHex("#00D66B");
            Application.Current.Resources["colorFalse"] = Color.FromHex("#D13830");
            Application.Current.Resources["colorTicked"] = Color.FromHex("#8FBCEB");
            Application.Current.Resources["buttonColorMenu"] = Color.FromHex("#356DA5");
            Application.Current.Resources["buttontextColorMenu"] = Color.FromHex("#FFFFFF");
            Application.Current.Resources["buttonColor"] = Color.FromHex("#D6D8D7");
            Application.Current.Resources["buttonTextColor"] = Color.FromHex("#000000");
            Application.Current.Resources["colorTransparent"] = Color.FromHex("#00000000");
            Application.Current.Resources["colorHighlight"] = Color.FromHex("#9E9E9E");
        }

        private static void Light() {
            if(Device.RuntimePlatform == Device.Android) {
                Application.Current.Resources["veryBigFont"] = 24.0;
                Application.Current.Resources["bigFont"] = 20.0;
                Application.Current.Resources["mediumFont"] = 16.0;
                Application.Current.Resources["smallFont"] = 14.0;
                Application.Current.Resources["iconSelectAll"] = "@drawable/ic_select_all_white_48dp.png";
                Application.Current.Resources["iconRefresh"] = "@drawable/ic_refresh_white_48dp.png";
                Application.Current.Resources["iconPlayArrow"] = "@drawable/ic_play_arrow_white_48dp.png";
                Application.Current.Resources["iconMenu"] = "@drawable/ic_menu_white_48dp.png";
                Application.Current.Resources["imageAppBG"] = "@drawable/app_bg.png";
            } else if(Device.RuntimePlatform == Device.UWP) {
                Application.Current.Resources["veryBigFont"] = 32.0;
                Application.Current.Resources["bigFont"] = 24.0;
                Application.Current.Resources["mediumFont"] = 18.0;
                Application.Current.Resources["smallFont"] = 16.0;
                //Application.Current.Resources["iconSelectAll"] = "";
                //Application.Current.Resources["iconRefresh"] = "";
                //Application.Current.Resources["iconPlayArrow"] = "";
                //Application.Current.Resources["iconMenu"] = "";
            } else if(Device.RuntimePlatform == Device.iOS) {
                Application.Current.Resources["veryBigFont"] = 24.0;
                Application.Current.Resources["bigFont"] = 20.0;
                Application.Current.Resources["mediumFont"] = 16.0;
                Application.Current.Resources["smallFont"] = 14.0;
                Application.Current.Resources["iconSelectAll"] = "ic_select_all_white_48pt.png";
                Application.Current.Resources["iconRefresh"] = "ic_refresh_white_48pt.png";
                Application.Current.Resources["iconPlayArrow"] = "ic_play_arrow_white_48pt.png";
                Application.Current.Resources["iconMenu"] = "ic_menu_white_48pt.png";
            }
            Application.Current.Resources["backgroundColor"] = Color.FromHex("#FFFFFF");
            Application.Current.Resources["barBackgroundColor"] = Color.FromHex("153E5E");
            Application.Current.Resources["barTextColor"] = Color.FromHex("#FFFFFF");
            Application.Current.Resources["seperatorColor"] = Color.FromHex("#BCBCBC");
            Application.Current.Resources["textColorHeader"] = Color.FromHex("#FFFFFF");
            Application.Current.Resources["textColorTitle"] = Color.FromHex("#153E5E");
            Application.Current.Resources["textColor"] = Color.FromHex("#000000");
            Application.Current.Resources["headerColor"] = Color.FromHex("#153E5E");
            Application.Current.Resources["colorCorrect"] = Color.FromHex("#00D66B");
            Application.Current.Resources["colorFalse"] = Color.FromHex("#D13830");
            Application.Current.Resources["colorTicked"] = Color.FromHex("#8FBCEB");
            Application.Current.Resources["buttonColorMenu"] = Color.FromHex("#153E5E");
            Application.Current.Resources["buttontextColorMenu"] = Color.FromHex("#FFFFFF");
            Application.Current.Resources["buttonColor"] = Color.FromHex("#D6D8D7");
            Application.Current.Resources["buttonTextColor"] = Color.FromHex("#000000");
            Application.Current.Resources["colorTransparent"] = Color.FromHex("#00000000");
            Application.Current.Resources["colorHighlight"] = Color.FromHex("#9E9E9E");
        }
    }
}
