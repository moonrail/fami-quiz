﻿using System;
using Xamarin.Forms;

namespace FaMI_Quiz {
    public static class ToastMessage { //TODO ios,uwp
        public static void Long(string message) {
            try {
                if(Device.RuntimePlatform == Device.Android) {
                    Console.WriteLine("DEBUG ToastMessage.Long:" + message);
                    DependencyService.Get<IAndroid>().LongAlert(message);
                } else if(Device.RuntimePlatform == Device.UWP) {
                } else if(Device.RuntimePlatform == Device.iOS) {
                }
            } catch { }
        }

        public static void Short(string message) {
            try {
                if(Device.RuntimePlatform == Device.Android) {
                    Console.WriteLine("DEBUG ToastMessage.Long:" + message);
                    DependencyService.Get<IAndroid>().ShortAlert(message);
                } else if(Device.RuntimePlatform == Device.UWP) {
                } else if(Device.RuntimePlatform == Device.iOS) {
                }
            } catch { }
        }
    }
}