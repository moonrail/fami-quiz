﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizMDPageMaster : ContentPage {

        public ListView ListView;
        public StackLayout StackLayout;

        public QuizMDPageMaster() {
            InitializeComponent();
            ListView = QuizQuestionListView;
            StackLayout = StackLayoutMain;
        }

        private async void ButtonQuizQuit_Clicked(object sender, EventArgs e) {
            if(VML.QuizVM.GetUnansweredCount != 0) {
                if(await DisplayAlert("Wirklich beenden?","Der gespielte Fortschritt wird erst nach Abschließen des Quiz gespeichert","Weiterspielen","Beenden")) return;
            }
            VML.QuizVM.ResetAnswers();
            VML.Notify();
            await Navigation.PopModalAsync(true);
        }
    }
}