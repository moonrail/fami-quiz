﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuizResultView : ContentPage {
        private QuizMDPage quizMDPage;

		public QuizResultView(QuizMDPage p) {
			InitializeComponent();
            quizMDPage = p;
            VML.QuizVM.SaveResults((this));
        }

        private async void ButtonQuizQuit_Clicked(object sender, EventArgs e) {
            VML.QuizVM.ResetAnswers();
            VML.Notify();
            await Navigation.PopModalAsync(true);
        }

        private void ButtonShowQuestions_Clicked(object sender, EventArgs e) {
            quizMDPage.IsPresented = true;
        }

        protected override bool OnBackButtonPressed() {
            if(!quizMDPage.IsPresented) quizMDPage.IsPresented = true;
            return true;
        }
    }
}