﻿using System;
using System.Collections.Generic;

using FaMI_Quiz.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuizCategoriesSelectorView : ContentPage {
        private bool SelectAll = true;
        private bool IsPressed = false;

		public QuizCategoriesSelectorView() {
			InitializeComponent();
		}

        private void ButtonMarkAll_Clicked(object sender, EventArgs e) {
            foreach(CategoryVM cVM in VML.MainCategoryVM.Categories) cVM.GotTicked = SelectAll;
            SelectAll = !SelectAll;
        }

        private async void ButtonQuizStart_Clicked(object sender, EventArgs e) {
            if(!IsPressed) {
                IsPressed = true;
                List<CategoryVM> categories = new List<CategoryVM>();
                foreach(CategoryVM cVM in VML.MainCategoryVM.Categories) {
                    if(cVM.GotTicked) categories.Add(cVM);
                }
                if(categories.Count > 0) {
                    try {
                        List<string> countArr = new List<string>();
                        int count = 0;
                        string choice = await DisplayActionSheet("Welche Fragen sollen gestellt werden?", "Abbruch", null, new string[] { "Unbeantwortete", "Falsch beantwortete", "Alle" });
                        bool? questionState;
                        switch(choice) {
                            case "Unbeantwortete":
                                questionState = null;
                                foreach(CategoryVM cVM in categories) count += cVM.GetUnansweredQuestionCount;
                                if(count == 0) await DisplayAlert("Info", "Keine unbeantworteten Fragen vorhanden", "OK");
                                break;
                            case "Falsch beantwortete":
                                questionState = false;
                                foreach(CategoryVM cVM in categories) count += cVM.GetIncorrectAnsweredQuestionCount;
                                if(count == 0) await DisplayAlert("Info", "Keine falsch beantworteten Fragen vorhanden", "OK");
                                break;
                            case "Alle":
                                questionState = true;
                                foreach(CategoryVM cVM in categories) count += cVM.GetQuestionCount;
                                if(count == 0) await DisplayAlert("Info", "Keine Fragen vorhanden", "OK");
                                break;
                            default:
                                IsPressed = false;
                                return;
                        }
                        if(count == 0) {
                            IsPressed = false;
                            return;
                        }
                        //Abfragen der Anzahl zu stellender Fragen
                        int[] num = new int[] { 5, 10, 15, 25, 35, 50 };
                        for(int i = 0; i < num.Length; ++i) if(count > num[i]) countArr.Add(num[i].ToString());
                        if(countArr.Count > 0) {
                            countArr.Add("Alle (" + count.ToString() + ")");
                            choice = await DisplayActionSheet("Wie viele Fragen sollen gestellt werden?", "Abbruch", null, countArr.ToArray());
                            if(Int32.TryParse(choice, out int tmp)) count = tmp;
#pragma warning disable CS1717 // Die Zuweisung wurde für dieselbe Variable durchgeführt.
                            else if(choice == "Alle (" + count.ToString() + ")") count = count; //dummy
#pragma warning restore CS1717 // Die Zuweisung wurde für dieselbe Variable durchgeführt.
                            else { //Abbruch und DefaultCancel
                                IsPressed = false;
                                return;
                            }
                        }
                        VML.QuizVM = new QuizVM(categories, questionState, count, false);
                        await Navigation.PushModalAsync(new QuizMDPage(), true);
                    } catch(Exception ex) {
                        await DisplayAlert(ex.Source, ex.Message, "Hmpf");
                    }
                } else ToastMessage.Short("Bitte Themen auswählen");
                IsPressed = false;
            }
        }

        private void ListViewQuizCategories_ItemTapped(object sender, ItemTappedEventArgs e) {
            if(e.Item == null) return;
            CategoryVM cVM = (CategoryVM)e.Item;
            cVM.GotTicked = !cVM.GotTicked;
            ListViewQuizCategories.SelectedItem = null;
        }

        protected override void OnAppearing() {
            bool b = false;
            foreach(CategoryVM cVM in VML.MainCategoryVM.Categories) {
                if(!cVM.GotTicked) {
                    b = true;
                    break;
                }
            }
            SelectAll = b;
            base.OnAppearing();
        }
    }
}