﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using FaMI_Quiz.ViewModel;

namespace FaMI_Quiz.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizMainCategoriesView : ContentPage {
        public QuizMainCategoriesView() {
            InitializeComponent();
        }

        private async void ButtonQuizStart_Clicked(object sender, EventArgs e) {
            await Navigation.PushAsync(new QuizMainCategoriesSelectorView(),true);
        }

        private async void ListViewMainQuizCategories_ItemTapped(object sender, ItemTappedEventArgs e) {
            if(e.Item == null) return;
            VML.MainCategoryVM = (MainCategoryVM)e.Item;
            ListViewMainQuizCategories.SelectedItem = null;
            await Navigation.PushAsync(new QuizCategoriesView(),true);
        }

        private async void ButtonMainCategoriesReset_Clicked(object sender, EventArgs e) {
            string choice = await DisplayActionSheet("Wirklich den Fortschritt aller Fächer und Themen zurücksetzen?", "Abbruch", null, new string[] { "Ja", "Nein" });
            if(choice == "Ja") {
                foreach(MainCategoryVM mcVM in VML.MainCategoryVMs) mcVM.ResetProgress();
                ToastMessage.Short("Fortschritt zurückgesetzt");
            }
        }
    }
}