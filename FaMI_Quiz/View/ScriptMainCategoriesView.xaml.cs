﻿using FaMI_Quiz.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScriptMainCategoriesView : ContentPage {
		public ScriptMainCategoriesView() {
			InitializeComponent();
		}

        private async void ListViewScriptMainCategories_ItemTapped(object sender, ItemTappedEventArgs e) {
            if(e.Item == null) return;
            VML.MainCategoryVM = (MainCategoryVM)e.Item;
            ListViewScriptMainCategories.SelectedItem = null;
            await Navigation.PushAsync(new ScriptCategoriesView(),true);
        }
    }
}