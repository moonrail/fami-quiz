﻿using FaMI_Quiz.ViewModel;
using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizMDPageDetail : ContentPage {
        private bool IsPressed = false;
        private QuestionVM questionVM;
        private QuizMDPage quizMDPage;
        public QuizMDPageDetail() {
            InitializeComponent();
            if(questionVM != null && questionVM.AreAnswersConfirmed) ButtonQuestionConfirm.Text = "Weiter";
        }
        public QuizMDPageDetail(QuestionVM qVM, QuizMDPage qMDP) {
            InitializeComponent();
            questionVM = qVM;
            quizMDPage = qMDP;
            BindingContext = questionVM;
            BuildAnswers();
            LabelCategory.Text = App.DBAccess.GetMainCategoryAndCategoryByCategoryID(questionVM.CategoryID);
        }

        private void BuildAnswers() {
            if(questionVM.IsMultipleChoice) {
                Label l = null;
                if(ConstVars.Setting_MultipleChoice_DisplayCount) l = new Label { Text = questionVM.GetNeededAnswerCount + " Antworten wählen" }; //genaue Ansage wieviele Antworten benoetigt werden
                else l = new Label { Text = "Mehrfachauswahl" }; //allgemein Ansage wenn Antworten ausstehend
                l.HorizontalOptions = LayoutOptions.CenterAndExpand;
                l.Style = (Style)Application.Current.Resources["Title"];
                StackLayoutAnswers.Children.Add(l);
            }
            for(int i = 0; i < questionVM.Answers.Count; ++i) {
                AnswerVM aVM = questionVM.Answers[i];
                if(aVM.Image != null) {
                    Image img = new Image {
                        Source = ImageSource.FromStream(() => new MemoryStream(aVM.Image)),
                        HeightRequest = Application.Current.MainPage.Height / 5,
                        Aspect = Aspect.AspectFit,
                        HorizontalOptions = LayoutOptions.Center
                    };
                    TapGestureRecognizer imageTap1 = new TapGestureRecognizer();
                    imageTap1.Tapped += (s, e) => {
                        OnTapGestureRecognizerTappedOneTime(s, e);
                    };
                    imageTap1.NumberOfTapsRequired = 1;
                    TapGestureRecognizer imageTap2 = new TapGestureRecognizer();
                    imageTap2.Tapped += (s, e) => {
                        OnTapGestureRecognizerTappedTwoTimes(s, e);
                    };
                    imageTap2.NumberOfTapsRequired = 2;
                    Frame f = new Frame {
                        BindingContext = aVM,
                        Content = img
                    };
                    f.Style = (Style)Application.Current.Resources["Frame"];
                    f.GestureRecognizers.Add(imageTap1);
                    f.GestureRecognizers.Add(imageTap2);
                    if(questionVM.AreAnswersConfirmed) { //Frage beantwortet
                        if(aVM.GotTicked) { //Frage beantwortet und Antwort gewaehlt
                            if(aVM.IsCorrect) PaintFrame(f, "correct");
                            else PaintFrame(f, "false");
                        } else if(aVM.IsCorrect && ConstVars.Setting_ShowCorrectAnswers) { //Frage beantwortet, Antwort korrekt, aber nicht gewaehlt, sowie Anzeige korrekter Antworten gewuenscht
                            bool animateDirection = true;
                            f.Animate(
                                "PulseColor",
                                x => {
                                    Color sourceColor = f.BackgroundColor;
                                    Color targetColor = Color.White;
                                    if(animateDirection) targetColor = (Color)Application.Current.Resources["colorCorrect"];
                                    else targetColor = (Color)Application.Current.Resources["colorTransparent"];
                                    double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                    double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                    double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                    double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                    f.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                },
                                length: 750,
                                easing: Easing.CubicOut,
                                repeat: () => {
                                    animateDirection = !animateDirection;
                                    return true;
                                }
                            );
                        }
                    } else if(aVM.GotTicked) PaintFrame(f, "ticked"); //Frage offen, Antwort gewaehlt
                    StackLayoutAnswers.Children.Add(f);
                } else {
                    Button b = new Button {
                        Style = (Style)Application.Current.Resources["Button"],
                        BindingContext = aVM
                    };
                    b.Clicked += ButtonAnswerQuestionSelect_Clicked;
                    b.SetBinding(Button.TextProperty, new Binding("Text"));
                    if(questionVM.AreAnswersConfirmed) { //Frage beantwortet
                        if(aVM.GotTicked) { //Frage beantwortet und Antwort gewaehlt
                            if(aVM.IsCorrect) PaintButton(b, "correct");
                            else PaintButton(b, "false");
                        } else if(aVM.IsCorrect && ConstVars.Setting_ShowCorrectAnswers) { //Frage beantwortet, Antwort korrekt, aber nicht gewaehlt, sowie Anzeige korrekter Antworten gewuenscht
                            bool animateDirection = true;
                            b.Animate(
                                "PulseColor",
                                x => {
                                    Color sourceColor = b.BackgroundColor;
                                    Color targetColor = Color.White;
                                    if(animateDirection) targetColor = (Color)Application.Current.Resources["colorCorrect"];
                                    else targetColor = (Color)Application.Current.Resources["buttonColor"];
                                    double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                    double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                    double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                    double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                    b.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                },
                                length: 750,
                                easing: Easing.CubicOut,
                                repeat: () => {
                                    animateDirection = !animateDirection;
                                    return true;
                                }
                            );
                        }
                    } else if(aVM.GotTicked) PaintButton(b, "ticked"); //Frage offen, Antwort gewaehlt
                    StackLayoutAnswers.Children.Add(b);
                }
            }
        }

        private async void OnTapGestureRecognizerTappedOneTime(object sender, EventArgs args) {
            try {
                ToastMessage.Short("Zweimal Tippen für Vollbild");
                if(!questionVM.AreAnswersConfirmed) { //verhindert erneute Antworten, nachdem einmalig geantwortet wurde
                    Frame f = (Frame)sender;
                    AnswerVM aVM = (AnswerVM)f.BindingContext;
                    if(!aVM.GotTicked && (!questionVM.GotAnswered || !questionVM.IsMultipleChoice)) {
                        if(!questionVM.IsMultipleChoice) DeselectAnswers(); //ermoeglicht direktes Aendern der Antwort ohne zuvor Abwaehlen zu muessen
                        aVM.GotTicked = true;
                        PaintFrame(f, "ticked");
                    } else {
                        aVM.GotTicked = false;
                        PaintFrame(f, "default");
                    }
                }
            } catch(Exception ex) {
                await DisplayAlert(ex.Source, ex.Message, "Meh");
            }
        }
        private void OnTapGestureRecognizerTappedTwoTimes(object sender, EventArgs args) {
            //TODO Show Image in Fullscreen
            Frame f = (Frame)sender;
            AnswerVM aVM = (AnswerVM)f.BindingContext;
            quizMDPage.Navigation.PushModalAsync(new QuizImageView(aVM),true);
            //ToastMessage.Short("Vollbild");
        }


        private async void ButtonAnswerQuestionSelect_Clicked(object sender, EventArgs e) {
            try {
                if(!questionVM.AreAnswersConfirmed) { //verhindert erneutes Antworten, nachdem einmalig geantwortet wurde
                    Button b = (Button)sender;
                    AnswerVM aVM = ((AnswerVM)(b).BindingContext);
                    if(!aVM.GotTicked && ((!questionVM.GotAnswered || !questionVM.IsMultipleChoice) || !ConstVars.Setting_MultipleChoice_DisplayCount )) {
                        if(!questionVM.IsMultipleChoice) DeselectAnswers(); //ermoeglicht direktes Aendern der Antwort ohne zuvor Abwaehlen zu muessen
                        aVM.GotTicked = true;
                        PaintButton(b, "ticked");
                    } else {
                        aVM.GotTicked = false;
                        PaintButton(b, "default");
                    }
                }
            } catch(Exception ex) {
                await DisplayAlert(ex.Source, ex.Message, "Meh");
            }
        }

        private void PaintButton(Button b, string state) {
            switch(state) {
                case "correct":
                    b.Style = (Style)Application.Current.Resources["ButtonCorrect"];
                    break;
                case "false":
                    b.Style = (Style)Application.Current.Resources["ButtonFalse"];
                    break;
                case "ticked":
                    b.Style = (Style)Application.Current.Resources["ButtonTicked"];
                    break;
                case "default":
                    b.Style = (Style)Application.Current.Resources["Button"];
                    break;
            }
        }

        private void PaintFrame(Frame f, string state) {
            switch(state) {
                case "correct":
                    f.Style = (Style)Application.Current.Resources["FrameCorrect"];
                    break;
                case "false":
                    f.Style = (Style)Application.Current.Resources["FrameFalse"];
                    break;
                case "ticked":
                    f.Style = (Style)Application.Current.Resources["FrameTicked"];
                    break;
                case "default":
                    f.Style = (Style)Application.Current.Resources["Frame"];
                    break;
            }
        }

        private async void ButtonQuestionConfirm_Clicked(object sender, EventArgs e) {
            if(!IsPressed) {
                IsPressed = true;
                if(questionVM.AreAnswersConfirmed) {
                    int index = VML.QuizVM.GetFirstUnconfirmedIndex;
                    if(index != -1) quizMDPage.NavigateTo(index);
                    else await Navigation.PushAsync(new QuizResultView(quizMDPage),true);
                } else {
                    if(ConstVars.Setting_MultipleChoice_DisplayCount && !questionVM.GotAnswered) { //genaue Ansage wieviele Antworten benoetigt werden
                        int count = questionVM.GetNeededAnswerCount - questionVM.GetAnswerCount;
                        if(count == 1) ToastMessage.Short("Eine Antwort verbleibend");
                        else ToastMessage.Short(count + " Antworten verbleibend");
                    } else if(questionVM.GotAnswered) {
                        questionVM.VerifyAnswersAndConfirm();
                        if(ConstVars.Setting_Sound) {
                            if(questionVM.GotCorrectlyAnswered) DependencyService.Get<IAndroid>().PlaySoundSuccess();
                            else DependencyService.Get<IAndroid>().PlaySoundFailure();
                        }
                        if(questionVM.GotCorrectlyAnswered) BarQuiz.Color = (Color)Application.Current.Resources["colorCorrect"];
                        else BarQuiz.Color = (Color)Application.Current.Resources["colorFalse"];
                        for(int i = 0; i < StackLayoutAnswers.Children.Count; ++i) {
                            if(StackLayoutAnswers.Children[i] is Button b) {
                                AnswerVM aVM = (AnswerVM)b.BindingContext;
                                if(aVM.GotTicked) {
                                    if(aVM.IsCorrect) {
                                        b.Animate(
                                            "ShiftColorTo",
                                            x => {
                                                Color sourceColor = b.BackgroundColor;
                                                Color targetColor = (Color)Application.Current.Resources["colorCorrect"];
                                                double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                                double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                                double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                                double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                                b.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                            },
                                            length: 2000,
                                            easing: Easing.CubicOut
                                        );
                                    } else {
                                        b.Animate(
                                            "ShiftColorTo",
                                            x => {
                                                Color sourceColor = b.BackgroundColor;
                                                Color targetColor = (Color)Application.Current.Resources["colorFalse"];
                                                double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                                double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                                double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                                double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                                b.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                            },
                                            length: 2000,
                                            easing: Easing.CubicOut
                                        );
                                    }
                                } else if(aVM.IsCorrect && ConstVars.Setting_ShowCorrectAnswers) { //Frage beantwortet, Antwort korrekt, aber nicht gewaehlt, sowie Anzeige korrekter Antworten gewuenscht
                                    bool animateDirection = true;
                                    b.Animate(
                                        "PulseColor",
                                        x => {                                                
                                            Color sourceColor = b.BackgroundColor;
                                            Color targetColor = Color.White;
                                            if(animateDirection) targetColor = (Color)Application.Current.Resources["colorCorrect"];
                                            else targetColor = (Color)Application.Current.Resources["buttonColor"];
                                            double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                            double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                            double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                            double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                            b.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                        },
                                        length: 750,
                                        easing: Easing.CubicOut,
                                        repeat: () => {
                                            animateDirection = !animateDirection;
                                            return true;
                                        }
                                    );
                                }
                            } else if(StackLayoutAnswers.Children[i] is Frame f) {
                                AnswerVM aVM = ((AnswerVM)(f).BindingContext);
                                if(aVM.GotTicked) {
                                    if(aVM.IsCorrect) {
                                        f.Animate(
                                            "ShiftColorTo",
                                            x => {
                                                Color sourceColor = f.BackgroundColor;
                                                Color targetColor = (Color)Application.Current.Resources["colorCorrect"];
                                                double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                                double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                                double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                                double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                                f.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                            },
                                            length: 1000,
                                            easing: Easing.CubicOut
                                        );
                                    } else {
                                        f.Animate(
                                            "ShiftColorTo",
                                            x => {
                                                Color sourceColor = f.BackgroundColor;
                                                Color targetColor = (Color)Application.Current.Resources["colorFalse"];
                                                double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                                double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                                double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                                double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                                f.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                            },
                                            length: 2000,
                                            easing: Easing.CubicOut
                                        );
                                    }
                                } else if(aVM.IsCorrect && ConstVars.Setting_ShowCorrectAnswers) { //Frage beantwortet, Antwort korrekt, aber nicht gewaehlt, sowie Anzeige korrekter Antworten gewuenscht
                                    bool animateDirection = true;
                                    f.Animate(
                                        "PulseColor",
                                        x => {
                                            Color sourceColor = f.BackgroundColor;
                                            Color targetColor = Color.White;
                                            if(animateDirection) targetColor = (Color)Application.Current.Resources["colorCorrect"];
                                            else targetColor = (Color)Application.Current.Resources["colorTransparent"];
                                            double red = sourceColor.R + (x * (targetColor.R - sourceColor.R));
                                            double green = sourceColor.G + (x * (targetColor.G - sourceColor.G));
                                            double blue = sourceColor.B + (x * (targetColor.B - sourceColor.B));
                                            double alpha = sourceColor.A + (x * (targetColor.A - sourceColor.A));
                                            f.BackgroundColor = Color.FromRgba(red, green, blue, alpha);
                                        },
                                        length: 750,
                                        easing: Easing.CubicOut,
                                        repeat: () => {
                                            animateDirection = !animateDirection;
                                            return true;
                                        }
                                    );
                                }
                            }
                        }

                        int index = VML.QuizVM.GetFirstUnconfirmedIndex;
                        ButtonQuestionConfirm.Text = "Weiter";
                    }
                }
                IsPressed = false;
            }
        }

        private void DeselectAnswers() {
            for(int i = 0; i < StackLayoutAnswers.Children.Count; ++i) {
                if(StackLayoutAnswers.Children[i] is Button b) {
                    AnswerVM aVM = (AnswerVM)b.BindingContext;
                    aVM.GotTicked = false;
                    PaintButton(b, "default");
                } else if(StackLayoutAnswers.Children[i] is Frame f) {
                    AnswerVM aVM = (AnswerVM)f.BindingContext;
                    aVM.GotTicked = false;
                    PaintFrame(f, "default");
                }
            }
        }

        protected override bool OnBackButtonPressed() {
            if(!quizMDPage.IsPresented) quizMDPage.IsPresented = true;
            return true;
        }
    }
}