﻿using System;
using Xamarin.Forms;

namespace FaMI_Quiz.View
{
	public partial class MainPage : ContentPage {
		public MainPage() {
			InitializeComponent();
        }

        private int backButtonPressedInterval = 300;
        private DateTime backButtonLastPressed = DateTime.Now;

        private async void StackLayoutQuiz_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            await Navigation.PushAsync(new QuizMainCategoriesView(), true);
        }

        private async void StackLayoutScript_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            await Navigation.PushAsync(new ScriptMainCategoriesView(), true);
        }

        private async void StackLayoutSettings_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            await Navigation.PushAsync(new SettingsView(), true);
        }

        private async void StackLayoutQuit_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            if(Device.RuntimePlatform == Device.Android) DependencyService.Get<IAndroid>().CloseApp(); //TODO ios,uwp
        }

        private async void AnimateStackLayout(object o) {
            if(o is StackLayout) {
                StackLayout s = o as StackLayout;
                Color targetColor = (Color)Application.Current.Resources["colorHighlight"];
                s.Animate(
                    "Blink",
                    x => {
                        if(x != 1) s.BackgroundColor = targetColor;
                        else s.SetDynamicResource(BackgroundColorProperty, "buttonColorMenu");
                    },
                    length: 250,
                    easing: Easing.Linear
                );
            }
        }

        protected override bool OnBackButtonPressed() {
            DateTime backButtonPressTime = DateTime.Now;
            if((backButtonPressTime - backButtonLastPressed).TotalMilliseconds <= backButtonPressedInterval) {
                if(Device.RuntimePlatform == Device.Android) DependencyService.Get<IAndroid>().CloseApp();
                //TODO ios,uwp
            } else ToastMessage.Short("Zweimal Zurück zum Beenden");
            backButtonLastPressed = backButtonPressTime;
            //return base.OnBackButtonPressed();
            return true;
        }

        protected override void OnSizeAllocated(double width, double height) {
            base.OnSizeAllocated(width, height);
            double newTileHeight = (width - 6) * 0.4; //Kacheln sind 40% der Breite groß und haben 6px Abstand zueinander
            double newImageHeight = height - newTileHeight * 2 - 52;
            if(newImageHeight >= 60) ImageLogo.HeightRequest = newImageHeight;
            else StackLayoutMain.Children.Remove(ImageLogo);
            StackLayoutQuiz.HeightRequest = newTileHeight;
            StackLayoutScript.HeightRequest = newTileHeight;
            StackLayoutSettings.HeightRequest = newTileHeight;
            StackLayoutQuit.HeightRequest = newTileHeight;
        }
    }
}
