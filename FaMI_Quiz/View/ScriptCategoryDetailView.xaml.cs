﻿using System;
using System.IO;
using System.Threading.Tasks;

using FaMI_Quiz.Provider;
using FaMI_Quiz.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;

namespace FaMI_Quiz.View {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScriptCategoryDetailView : ContentPage {
        public ScriptCategoryDetailView() {
            InitializeComponent();
            Title = VML.MainCategoryVM.Name + ": " + VML.CategoryVM.Name;
        }

        private async void ListViewScripts_ItemTapped(object sender, ItemTappedEventArgs e) {
            if(e.Item == null) return;
            ScriptVM sVM = (ScriptVM)e.Item;
            ListViewScripts.SelectedItem = null;
            if(Device.RuntimePlatform == Device.Android) {
                try {
                    sVM = ScriptProvider.GetScriptPDF(sVM);
                    string localPath = string.Empty;
                    if(Device.RuntimePlatform == Device.Android) {
                        IAndroid dependency = DependencyService.Get<IAndroid>();
                        string filename = sVM.PDF_Name;
                        if(!filename.ToLower().EndsWith(".pdf")) filename += ".pdf";
                        bool gotpermission = await GetPermission();
                        if(gotpermission) {
                            bool existing = Task.Run(() => dependency.CheckIfFileExists(filename, sVM.PDF.Length)).Result;
                            localPath = dependency.GetFilePath(filename);
                            if(!existing) {
                                Stream pdfStream = new MemoryStream(sVM.PDF);
                                await Task.Run(() => dependency.SaveFileToDisk(pdfStream, localPath));
                            }
                            if(ConstVars.Setting_PDFextern) { //extern
                                try {
                                    await dependency.OpenPDFExternally(localPath);
                                } catch(Exception ex) {
                                    if(await DisplayAlert("Fehler beim Öffnen der PDF", "PDF kann nicht extern geöffnet werden - ein Grund könnte sein, dass keine passende App installiert ist.", "Intern öffnen","OK")) {
                                        await Navigation.PushAsync(new ScriptPDFView(localPath), true); //intern
                                    }
                                }
                            } else await Navigation.PushAsync(new ScriptPDFView(localPath), true); //intern
                        }
                    }
                } catch(Exception ex) {
                    await DisplayAlert("Computer sagt nein", ex.Message, "OK");
                }
            }
        }

        private async Task<bool> GetPermission() {
            try {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if(status != PermissionStatus.Granted) {
                    if(await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage)) {
                        await DisplayAlert("Berechtigung fehlt", "Um Nachschlagewerke anzeigen zu können, muss die App Berechtigung auf den internen Speicher erhalten", "OK");
                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                    if(results.ContainsKey(Permission.Storage)) status = results[Permission.Storage];
                }

                if(status == PermissionStatus.Granted) return true;
                else if(status != PermissionStatus.Unknown) {
                    await DisplayAlert("Berechtigung verweigert", "Kann Nachschlagewerk auf Grund fehlender Berechtigung auf den internen Speicher nicht öffnen", "OK");
                    return false;
                }
            } catch(Exception ex) {
                await DisplayAlert("Computer sagt nein", ex.Message, "OK");
                return false;
            }
            return false;
        }
    }
}