﻿using FaMI_Quiz.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScriptCategoriesView : ContentPage {
		public ScriptCategoriesView() {
			InitializeComponent();
		}

        private async void ListViewScriptCategories_ItemTapped(object sender, ItemTappedEventArgs e) {
            if(e.Item == null) return;
            VML.CategoryVM = ((CategoryVM)e.Item);
            ListViewScriptCategories.SelectedItem = null;
            await Navigation.PushAsync(new ScriptCategoryDetailView(),true);
        }
    }
}