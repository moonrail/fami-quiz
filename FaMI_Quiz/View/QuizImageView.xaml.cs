﻿using System.IO;

using FaMI_Quiz.ViewModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuizImageView : ContentPage {
		public QuizImageView(AnswerVM aVM) {
			InitializeComponent();
            ImageView.Source = ImageSource.FromStream(() => new MemoryStream(aVM.Image));
        }
	}
}