﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsAboutUsView : ContentPage {
		public SettingsAboutUsView () {
			InitializeComponent ();
		}
	}
}