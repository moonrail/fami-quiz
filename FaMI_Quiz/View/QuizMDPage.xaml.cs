﻿using System;
using FaMI_Quiz.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizMDPage : MasterDetailPage {
        public QuizMDPage() {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += QuizQuestionsListView_ItemSelected;
            NavigateTo(0);
        }

        private void QuizQuestionsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e) {
            QuestionVM item = (QuestionVM)e.SelectedItem;
            MasterPage.ListView.SelectedItem = null;
            if(item == null) return;
            Page page = (Page)Activator.CreateInstance(item.TargetType, item, this);
            page.Title = "Frage " + item.Index + " von " + VML.QuizVM.GetQuestionCount;
            Detail = new NavigationPage(page);
            IsPresented = false;
        }

        public void NavigateTo(int index) {
            QuestionVM item = VML.QuizVM.GetQuestions[index];
            if(item == null) return;
            Page page = (Page)Activator.CreateInstance(item.TargetType, item, this);
            page.Title = "Frage " + item.Index + " von " + VML.QuizVM.GetQuestionCount;
            Detail = new NavigationPage(page);
            IsPresented = false;
        }

        private void MasterDetailPage_IsPresentedChanged(object sender, EventArgs e) {
            if(IsPresented) {
                if(VML.QuizVM.GetUnansweredCount == 0) {
                    if(!(MasterPage.StackLayout.Children[MasterPage.StackLayout.Children.Count - 2] is Button)) { //Fuege Ergebnis-Button hinzu, wenn alle Antworten bestaetigt
                        Button b = new Button {
                            Text = "Ergebnis",
                            Style = (Style)App.Current.Resources["ButtonMenu"]
                        };
                        b.Clicked += ButtonResults_Clicked;
                        MasterPage.StackLayout.Children.Insert((MasterPage.StackLayout.Children.Count - 1), b);
                    }
                }
            }
        }

        private void ButtonResults_Clicked(object sender, EventArgs args) {
            Detail = new NavigationPage(new QuizResultView(this));
            IsPresented = false;
        }
    }
}