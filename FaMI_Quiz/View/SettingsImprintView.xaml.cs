﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsImprintView : ContentPage {
		public SettingsImprintView () {
			InitializeComponent ();
		}

        public void LabelSourceGit_Tapped(object sender, EventArgs e) {
            Device.OpenUri(new Uri("https://gitlab.com/moonrail/fami-quiz"));
        }

        public void LabelXamarin_Tapped(object sender, EventArgs e) {
            Device.OpenUri(new Uri("https://www.xamarin.com/license"));
        }

        public async void LabelPDFjs_Tapped(object sender, EventArgs e) {
            Device.OpenUri(new Uri("https://github.com/mozilla/pdf.js/blob/master/LICENSE"));
        }

        public async void LabelSQLITE_Tapped(object sender, EventArgs e) {
            if(await DisplayAlert("SQLite.Net-PCL - MIT License", "Copyright (c) 2012 Krueger Systems, Inc." + Environment.NewLine + "Copyright (c) 2013 Øystein Krog (oystein.krog@gmail.com)" + Environment.NewLine + "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:" + Environment.NewLine + "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software." + Environment.NewLine + "THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.", "Webseite", "OK")) {
                Device.OpenUri(new Uri("https://github.com/oysteinkrog/SQLite.Net-PCL"));
            }
        }

        public async void LabelSQLITEEXT_Tapped(object sender, EventArgs e) {
            if(await DisplayAlert("SQLiteNetExtensions - MIT License", "Copyright (C) 2013 TwinCoders S.L." + Environment.NewLine + "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:" + Environment.NewLine + "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software." +Environment.NewLine+"THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.", "Webseite", "OK")) {
                Device.OpenUri(new Uri("https://bitbucket.org/twincoders/sqlite-net-extensions"));
            }
        }

        public async void LabelSimpleAudioPlayer_Tapped(object sender, EventArgs e) {
            if(await DisplayAlert("Xam.Plugin.SimpleAudioPlayer - MIT License", "Copyright (c) 2017 Adrian Stevens" + Environment.NewLine + "Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files(the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and / or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:" + Environment.NewLine + "The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software." +Environment.NewLine+"THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.", "Webseite", "OK")) {
                Device.OpenUri(new Uri("https://github.com/adrianstevens/Xamarin-Plugins"));
            }
        }
    }
}