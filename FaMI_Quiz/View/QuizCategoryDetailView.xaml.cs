﻿using System;
using System.Collections.Generic;
using FaMI_Quiz.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class QuizCategoryDetailView : ContentPage {
        private bool IsPressed = false;

		public QuizCategoryDetailView() {
			InitializeComponent();
        }

        private async void ButtonQuizStart_Clicked(object sender, EventArgs e) {
            if(!IsPressed) {
                IsPressed = true;
                try {
                    CategoryVM cVM = VML.CategoryVM;
                    List<string> countArr = new List<string>();
                    int count = -1;
                    string choice = await DisplayActionSheet("Welche Fragen sollen gestellt werden?", "Abbruch", null, new string[] { "Unbeantwortete", "Falsch beantwortete", "Alle" });
                    bool? questionState;
                    switch(choice) {
                        case "Unbeantwortete":
                            questionState = null;
                            count = cVM.GetUnansweredQuestionCount;
                            if(count == 0) await DisplayAlert("Info", "Keine unbeantworteten Fragen vorhanden", "OK");
                            break;
                        case "Falsch beantwortete":
                            questionState = false;
                            count = cVM.GetIncorrectAnsweredQuestionCount;
                            if(count == 0) await DisplayAlert("Info", "Keine falsch beantworteten Fragen vorhanden", "OK");
                            break;
                        case "Alle":
                            questionState = true;
                            count = cVM.GetQuestionCount;
                            if(count == 0) await DisplayAlert("Info", "Keine Fragen vorhanden", "OK");
                            break;
                        default:
                            IsPressed = false;
                            return;
                    }
                    if(count == 0) {
                        IsPressed = false;
                        return;
                    }
                    //Abfragen der Anzahl zu stellender Fragen
                    int[] num = new int[] { 5, 10, 15, 25, 35, 50 };
                    for(int i = 0; i < num.Length; ++i) if(count > num[i]) countArr.Add(num[i].ToString());
                    if(countArr.Count > 0) {
                        countArr.Add("Alle (" + count.ToString() + ")");
                        choice = await DisplayActionSheet("Wie viele Fragen sollen gestellt werden?", "Abbruch", null, countArr.ToArray());
                        if(Int32.TryParse(choice, out int tmp)) count = tmp;
#pragma warning disable CS1717 // Die Zuweisung wurde für dieselbe Variable durchgeführt.
                        else if(choice == "Alle (" + count.ToString() + ")") count = count; //dummy
#pragma warning restore CS1717 // Die Zuweisung wurde für dieselbe Variable durchgeführt.
                        else { //Abbruch und DefaultCancel
                            IsPressed = false;
                            return;
                        }
                    }
                    VML.QuizVM = new QuizVM(new List<CategoryVM>() { cVM }, questionState, count, false);
                    await Navigation.PushModalAsync(new QuizMDPage(), true);
                } catch(Exception ex) {
                    await DisplayAlert(ex.Source, ex.Message, "Hmpf");
                }
                IsPressed = false;
            }
        }

        private async void ButtonCategoryReset_Clicked(object sender, EventArgs e) {
            string choice = await DisplayActionSheet("Wirklich den Fortschritt dieses Themas zurücksetzen?", "Abbruch", null, new string[] { "Ja", "Nein" });
            if(choice == "Ja") {
                VML.CategoryVM.ResetProgress();
                ToastMessage.Short("Fortschritt zurückgesetzt");
            }
        }
    }
}