﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ScriptPDFView : ContentPage {
		public ScriptPDFView(string localFilePath) {
            InitializeComponent();
            PDFWebViewControl.Uri = localFilePath;
        }
	}
}