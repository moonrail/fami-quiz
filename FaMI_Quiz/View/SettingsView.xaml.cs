﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;

namespace FaMI_Quiz.View {
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SettingsView : ContentPage {
		public SettingsView() {
			InitializeComponent();
            SwitchSound.IsToggled = ConstVars.Setting_Sound;
            SwitchPDFextern.IsToggled = ConstVars.Setting_PDFextern;
            SwitchDarkTheme.IsToggled = ConstVars.Setting_DarkTheme;
            SwitchMultipleChoiceDisplayCount.IsToggled = ConstVars.Setting_MultipleChoice_DisplayCount;
            SwitchShowCorrectAnswers.IsToggled = ConstVars.Setting_ShowCorrectAnswers;
            string version = "n.v.";
            if(Device.RuntimePlatform == Device.Android) version = DependencyService.Get<IAndroid>().GetVersion();
            LabelVersionString.Text = version + " / " + ConstVars.DBVersion_Current;
        }

        private void StackLayoutSound_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(StackLayoutSound);
            SwitchSound.IsToggled = !SwitchSound.IsToggled;
        }

        private void StackLayoutShowCorrectAnswers_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(StackLayoutShowCorrectAnswers);
            SwitchShowCorrectAnswers.IsToggled = !SwitchShowCorrectAnswers.IsToggled;
        }

        private void StackLayoutMultipleChoiceDisplayCount_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(StackLayoutMultipleChoiceDisplayCount);
            SwitchMultipleChoiceDisplayCount.IsToggled = !SwitchMultipleChoiceDisplayCount.IsToggled;
        }

        private void StackLayoutDarkTheme_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(StackLayoutDarkTheme);
            SwitchDarkTheme.IsToggled = !SwitchDarkTheme.IsToggled;
        }

        private void StackLayoutPDFextern_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(StackLayoutPDFextern);
            SwitchPDFextern.IsToggled = !SwitchPDFextern.IsToggled;
        }

        private void SwitchSound_Toggled(object sender, ToggledEventArgs e) {
            if(SwitchSound.IsToggled != ConstVars.Setting_Sound) {
                setting se = App.DBAccess.GetSetting(ConstVars.Setting_Sound_Name);
                if(se != null) {
                    if(SwitchSound.IsToggled) se.se_value = "1";
                    else se.se_value = "0";
                    ConstVars.Setting_Sound = SwitchSound.IsToggled;
                    App.DBAccess.UpdateSetting(se);
                }
            }
        }

        private void SwitchShowCorrectAnswers_Toggled(object sender, ToggledEventArgs e) {
            if(SwitchShowCorrectAnswers.IsToggled != ConstVars.Setting_ShowCorrectAnswers) {
                setting se = App.DBAccess.GetSetting(ConstVars.Setting_ShowCorrectAnswers_Name);
                if(se != null) {
                    if(SwitchShowCorrectAnswers.IsToggled) se.se_value = "1";
                    else se.se_value = "0";
                    ConstVars.Setting_ShowCorrectAnswers = SwitchShowCorrectAnswers.IsToggled;
                    App.DBAccess.UpdateSetting(se);
                }
            }
        }

        private void SwitchMultipleChoiceDisplayCount_Toggled(object sender, ToggledEventArgs e) {
            if(SwitchMultipleChoiceDisplayCount.IsToggled != ConstVars.Setting_MultipleChoice_DisplayCount) {
                setting se = App.DBAccess.GetSetting(ConstVars.Setting_MultipleChoice_DisplayCount_Name);
                if(se != null) {
                    if(SwitchMultipleChoiceDisplayCount.IsToggled) se.se_value = "1";
                    else se.se_value = "0";
                    ConstVars.Setting_MultipleChoice_DisplayCount = SwitchMultipleChoiceDisplayCount.IsToggled;
                    App.DBAccess.UpdateSetting(se);
                }
            }
        }

        private void SwitchDarkTheme_Toggled(object sender, ToggledEventArgs e) {
            if(SwitchDarkTheme.IsToggled != ConstVars.Setting_DarkTheme) {
                setting se = App.DBAccess.GetSetting(ConstVars.Setting_DarkTheme_Name);
                if(se != null) {
                    if(SwitchDarkTheme.IsToggled) se.se_value = "1";
                    else se.se_value = "0";
                    ConstVars.Setting_DarkTheme = SwitchDarkTheme.IsToggled;
                    App.DBAccess.UpdateSetting(se);
                    ThemeSwitcher.Switch(false);
                }
            }
        }

        private void SwitchPDFextern_Toggled(object sender, ToggledEventArgs e) {
            if(SwitchPDFextern.IsToggled != ConstVars.Setting_PDFextern) {
                setting se = App.DBAccess.GetSetting(ConstVars.Setting_PDFextern_Name);
                if(se != null) {
                    if(SwitchPDFextern.IsToggled) se.se_value = "1";
                    else se.se_value = "0";
                    ConstVars.Setting_PDFextern = SwitchPDFextern.IsToggled;
                    App.DBAccess.UpdateSetting(se);
                }
            }
        }

        private async void StackLayoutResetDB_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            string choice = await DisplayActionSheet("Soll der gesamte Fortschritt zurückgesetzt werden?", "Abbruch", null, new string[] { "Ja", "Nein" });
            if(choice == "Ja") {
                try {
                    App.DBAccess.ResetProgress();
                    VML.ReloadMainCategoryVMs();
                    ToastMessage.Long("Fortschritt zurückgesetzt");
                } catch(Exception ex) {
                    await DisplayAlert("Fehler beim Löschen des Fortschritts", ex.Message, "OK");
                }
            }

        }

        private async void StackLayoutExportDB_Tapped(object sender, EventArgs e) {
            AnimateStackLayout((StackLayout)sender);
            try {
                Dictionary<int, int> question_id_progress = App.DBAccess.GetQuestionProgress();
                MemoryStream ms = new MemoryStream();
                StreamWriter sw = new StreamWriter(ms);
                foreach(KeyValuePair<int, int> kvp in question_id_progress) sw.WriteLine(kvp.Key + "|" + kvp.Value);
                sw.Close();
                DependencyService.Get<IAndroid>().WriteFile(ms.ToArray(), "FaMi_Sicherung_"+DateTime.Now.ToString("yyyyMMdd_HHmmss")+".csv");
                ToastMessage.Long("Sicherung im Ordner FaMI erstellt");
            } catch(Exception ex) {
                await DisplayAlert("Fehler beim Sichern", ex.Message, "Alrighty");
            }
        }

        private async void StackLayoutImportDB_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            try {
                IFilePicker crossFilePicker = CrossFilePicker.Current;
                FileData data = await crossFilePicker.PickFile();
                if(data != null && !string.IsNullOrEmpty(data.FilePath)) {
                    if(!data.FileName.EndsWith(".csv")) {
                        await DisplayAlert("Falsche Datei", "Die angegebene Datei ist keine Sicherung"+Environment.NewLine+"Bitte *.csv-Datei angeben"+Environment.NewLine+"FaMI-Sicherungen liegen standardmäßig im Ordner FaMI im externen Speicher", "OK");
                    } else {
                        Dictionary<int, int> question_id_progress = new Dictionary<int, int>();
                        using(StreamReader sr = new StreamReader(new MemoryStream(data.DataArray), Encoding.UTF8)) {
                            string line = string.Empty;
                            sr.ReadLine();
                            while((line = sr.ReadLine()) != null) {
                                string[] values = line.Split('|');
                                if(values.Length == 2 && int.TryParse(values[0],out int id) & int.TryParse(values[1], out int progress)) question_id_progress.Add(id, progress);
                            }
                        }
                        if(question_id_progress.Count == 0) await DisplayActionSheet("Keine Daten", "Es wurden keine wiederherstellbaren Daten in der Sicherung gefunden", "OK");
                        else {
                            if(!App.DBAccess.RestoreProgress(question_id_progress)) await DisplayActionSheet("Datenbank-Fehler", "Beim Hinzufügen des Fortschritts zur Datenbank ist ein Fehler aufgetreten", "OK");
                            else ToastMessage.Long("Wiederherstellung erfolgreich abgeschlossen");
                            VML.ReloadMainCategoryVMs();
                        }
                    }
                }
            } catch(Exception ex) {
                await DisplayAlert("Fehler beim Wiederherstellen", ex.Message, "Alrighty");
            }
        }

        private async void StackLayoutAboutUs_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            await Navigation.PushAsync(new SettingsAboutUsView(),true);
        }

        private async void StackLayoutImprint_Tapped(object sender, EventArgs e) {
            AnimateStackLayout(sender);
            await Navigation.PushAsync(new SettingsImprintView(),true);
        }

        private void AnimateStackLayout(object o) {
            if(o is StackLayout) {
                StackLayout s = o as StackLayout;
                Color sourceColor = s.BackgroundColor;
                Color targetColor = (Color)Application.Current.Resources["colorHighlight"];
                s.Animate(
                    "Blink",
                    x => {
                        if(x != 1) s.BackgroundColor = targetColor;
                        else s.BackgroundColor = sourceColor;
                    },
                    length: 250,
                    easing: Easing.Linear
                );
            }
        }
    }
}