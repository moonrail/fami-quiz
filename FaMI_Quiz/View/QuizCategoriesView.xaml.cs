﻿using System;
using FaMI_Quiz.ViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaMI_Quiz.View {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuizCategoriesView : ContentPage {

        public QuizCategoriesView() {
            InitializeComponent();
        }

        private async void ListViewQuizCategories_ItemTapped(object sender, ItemTappedEventArgs e) {
            if(e.Item == null) return;
            VML.CategoryVM = (CategoryVM)e.Item;
            ListViewQuizCategories.SelectedItem = null;
            await Navigation.PushAsync(new QuizCategoryDetailView(),true);
        }

        private async void ButtonQuizStart_Clicked(object sender, EventArgs e) {
            await Navigation.PushAsync(new QuizCategoriesSelectorView(),true);
        }

        private async void ButtonMainCategoryReset_Clicked(object sender, EventArgs e) {
            string choice = await DisplayActionSheet("Wirklich den Fortschritt dieses Fachs und seiner Themen zurücksetzen?", "Abbruch", null, new string[] { "Ja", "Nein" });
            if(choice == "Ja") {
                VML.MainCategoryVM.ResetProgress();
                ToastMessage.Short("Fortschritt zurückgesetzt");
            }
        }
    }
}
