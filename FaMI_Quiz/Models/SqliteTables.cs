﻿using SQLite;
using SQLiteNetExtensions.Attributes;

namespace FaMI_Quiz {
    [Table("maincategory")]
    public class MainCategory {
        [PrimaryKey, AutoIncrement]
        public int mc_id { get; set; }
        [NotNull, Column("mc_name")]
        public string mc_name { get; set; }
        [Column("mc_description")]
        public string mc_description { get; set; }
    }
    [Table("category")]
    public class Category {
        [PrimaryKey, AutoIncrement]
        public int c_id { get; set; }
        [NotNull, Column("c_name")]
        public string c_name { get; set; }
        [Column("c_description")]
        public string c_description { get; set; }
        [ForeignKey(typeof(MainCategory)), NotNull, Column("c_maincategory_id")]
        public int c_maincategory_id { get; set; }
    }
    [Table("script")]
    public class Script {
        [PrimaryKey, AutoIncrement]
        public int s_id { get; set; }
        [NotNull, Column("s_pdf")]
        public byte[] s_pdf { get; set; }
        [ForeignKey(typeof(Category)), NotNull, Column("s_category_id")]
        public int s_category_id { get; set; }
        [NotNull, Column("s_name")]
        public string s_name { get; set; }
        [Column("s_description")]
        public string s_description { get; set; }
        [Column("s_order")]
        public int s_order { get; set; }
        [Column("s_pdf_name")]
        public string s_pdf_name { get; set; }

    }
    [Table("question")]
    public class Question {
        [PrimaryKey, AutoIncrement]
        public int q_id { get; set; }
        [NotNull, Column("q_text")]
        public string q_text { get; set; }
        [Column("q_points")]
        public int q_points { get; set; }
        [ForeignKey(typeof(Category)), NotNull, Column("q_category_id")]
        public int q_category_id { get; set; }
        [Column("q_is_multiple_choice")]
        public int q_is_multiple_choice { get; set; }
        [Column("q_is_answered")]
        public int q_is_answered { get; set; }
    }
    [Table("answer")]
    public class Answer {
        [PrimaryKey, AutoIncrement]
        public int a_id { get; set; }
        [NotNull, Column("a_text")]
        public string a_text { get; set; }
        [ForeignKey(typeof(Question)), NotNull, Column("a_question_id")]
        public int a_question_id { get; set; }
        [Column("a_image")]
        public byte[] a_image { get; set; }
        [Column("a_is_correct")]
        public int a_is_correct { get; set; }
    }
    [Table("setting")]
    public class setting {
        [PrimaryKey, Column("se_name")]
        public string se_name { get; set; }
        [Column("se_value")]
        public string se_value { get; set; }
    }
}
