﻿namespace FaMI_Quiz {
    public static class ConstVars {
        public const string DB_Name = "FaMI_Quiz.db3"; //mit Dateiendung, z.B. DBname.db3
        public const string Setting_Sound_Name = "sound"; //0 für nein, 1 für ja
        public const string Setting_PDFextern_Name = "pdfextern"; //0 für nein, 1 für ja
        public const string Setting_DarkTheme_Name = "darktheme"; //0 für nein, 1 für ja
        public const string Setting_MultipleChoice_DisplayCount_Name = "multiplechoice_displaycount"; //0 für nein, 1 für ja
        public const string Setting_ShowCorrectAnswers_Name = "showcorrectanswers"; //0 für nein, 1 für ja
        public const string Setting_DBVersion_Name = "dbversion";
        public static bool Setting_Sound = true;
        public static bool Setting_PDFextern = true;
        public static bool Setting_DarkTheme = false;
        public static bool Setting_MultipleChoice_DisplayCount = true;
        public static bool Setting_ShowCorrectAnswers = true;
        public static int DBVersion_Current = -1; //Wird zur Laufzeit mit der Version der installierten DB gesetzt
        public static int DBVersion_Build = 13; //Wenn neuer als installierte DB, wird DB migriert - Hier hochsetzen, wenn neue DB-Version ausgerollt wird, d.h.: Der Wert muss dem in DB/setting/db_version  entsprechen
    }
}