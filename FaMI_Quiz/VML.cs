﻿using System.Collections.ObjectModel;
using FaMI_Quiz.ViewModel;
using FaMI_Quiz.Provider;

namespace FaMI_Quiz {
    public static class VML {

        static ObservableCollection<MainCategoryVM> mainCategoryVMs;
        public static ObservableCollection<MainCategoryVM> MainCategoryVMs => mainCategoryVMs ?? (mainCategoryVMs = MainCategoryProvider.LoadMainCategories(true));

        static MainCategoryVM mainCategoryVM;
        public static MainCategoryVM MainCategoryVM { get { return mainCategoryVM; } set { mainCategoryVM = value; } }

        static CategoryVM categoryVM;
        public static CategoryVM CategoryVM { get { return categoryVM; } set { categoryVM = value; } }

        static QuizVM quizVM;
        public static QuizVM QuizVM { get { return quizVM; } set { quizVM = value; } }

        public static void ReloadMainCategoryVMs() {
            mainCategoryVMs = MainCategoryProvider.LoadMainCategories(true);
        }

        public static void Notify() {
            foreach(MainCategoryVM mVM in mainCategoryVMs) {
                mVM.Recalculate_and_Notify();
                foreach(CategoryVM cVM in mVM.Categories) cVM.Recalculate_and_Notify();
            }
            if(MainCategoryVM != null) MainCategoryVM.Recalculate_and_Notify();
            if(CategoryVM != null) CategoryVM.Recalculate_and_Notify();
        }
    }
}