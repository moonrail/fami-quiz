﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using FaMI_Quiz.ViewModel;

namespace FaMI_Quiz.Provider {
    public static class AnswerProvider {
        public static ObservableCollection<AnswerVM> LoadAnswers(QuestionVM question, bool withImages) {
            ObservableCollection<AnswerVM> answers = new ObservableCollection<AnswerVM>();
            List<Answer> ans = App.DBAccess.GetAnswersByQuestionWithImages(question);
            foreach(Answer a in ans) answers.Add(new AnswerVM(a));
            return answers;
        }
    }
}
