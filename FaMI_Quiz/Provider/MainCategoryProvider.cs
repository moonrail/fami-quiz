﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using FaMI_Quiz.ViewModel;

namespace FaMI_Quiz.Provider {
    public static class MainCategoryProvider {
        public static ObservableCollection<MainCategoryVM> LoadMainCategories(bool withChildren) {
            ObservableCollection<MainCategoryVM> mainCategories = new ObservableCollection<MainCategoryVM>();
            List<MainCategory> mcs = App.DBAccess.GetAllMainCategories();
            foreach(MainCategory mc in mcs) {
                MainCategoryVM mcVM = new MainCategoryVM(mc);
                if(withChildren) mcVM.Categories = CategoryProvider.LoadCategories(mcVM, withChildren);
                mainCategories.Add(mcVM);
            }
            return mainCategories;
        }
    }
}