﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using FaMI_Quiz.ViewModel;

namespace FaMI_Quiz.Provider {
    public static class ScriptProvider {
        public static ObservableCollection<ScriptVM> LoadScripts(CategoryVM category) {
            ObservableCollection<ScriptVM> scripts = new ObservableCollection<ScriptVM>();
            List<Script> ss = App.DBAccess.GetScriptsByCategory(category);
            foreach(Script s in ss) scripts.Add(new ScriptVM(s));
            return scripts;
        }

        public static ScriptVM GetScriptPDF(ScriptVM sVM) {
            Script s = App.DBAccess.GetScript(sVM.ID);
            if(s != null) sVM.PDF = s.s_pdf;
            return sVM;
        }
    }
}