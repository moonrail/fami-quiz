﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FaMI_Quiz.ViewModel;

namespace FaMI_Quiz.Provider {
    public static class QuestionProvider {
        public static ObservableCollection<QuestionVM> LoadQuestions(CategoryVM category, bool withChildren) {
            ObservableCollection<QuestionVM> questions = new ObservableCollection<QuestionVM>();
            List<Question> qs = App.DBAccess.GetQuestionsByCategory(category);
            foreach(Question q in qs) {
                QuestionVM qVM = new QuestionVM(q);
                qVM.Answers = AnswerProvider.LoadAnswers(qVM, false);
                questions.Add(qVM);
            }
            return questions;
        }
        public static ObservableCollection<QuestionVM> LoadRandomQuestionsWithAnswersFromDB(List<CategoryVM> categories, bool? questionState, int count) {
            ObservableCollection<QuestionVM> questions = new ObservableCollection<QuestionVM>();
            List<Question> qs = App.DBAccess.GetQuestionsByCategory(categories, questionState, count);
            foreach(Question q in qs) {
                QuestionVM qVM = new QuestionVM(q);
                qVM.Answers = AnswerProvider.LoadAnswers(qVM, true);
                questions.Add(qVM);
            }
            return questions;
        }
        public static ObservableCollection<QuestionVM> LoadRandomQuestionsWithAnswersFromDB(List<MainCategoryVM> mainCategories, bool? questionState, int count) {
            ObservableCollection<QuestionVM> questions = new ObservableCollection<QuestionVM>();
            List<Question> qs = App.DBAccess.GetQuestionsByCategory(mainCategories, questionState, count);
            foreach(Question q in qs) {
                QuestionVM qVM = new QuestionVM(q);
                qVM.Answers = AnswerProvider.LoadAnswers(qVM, true);
                questions.Add(qVM);
            }
            return questions;
        }
        public static ObservableCollection<QuestionVM> LoadRandomQuestionsWithAnswers(List<MainCategoryVM> mainCategories, bool? questionState, int count) {
            ObservableCollection<QuestionVM> questions = new ObservableCollection<QuestionVM>();
            foreach(MainCategoryVM mVM in mainCategories) {
                foreach(CategoryVM cVM in mVM.Categories) {
                    foreach(QuestionVM qVM in cVM.Questions) {
                        if(questionState == true) questions.Add(qVM); //alle, wenn nicht nur unbeantwortete
                        else if(qVM.IsAnswered == questionState) questions.Add(qVM); //nur wenn unbeantwortet/falsch beantwortet
                    }
                }
            }
            questions = Shuffle(questions);
            while(questions.Count > count) questions.RemoveAt(questions.Count - 1); //entferne Fragen bis gewuenschte Anzahl erreicht
            foreach(QuestionVM qVM in questions) qVM.Answers = AnswerProvider.LoadAnswers(qVM, true);
            return questions;
        }
        public static ObservableCollection<QuestionVM> LoadRandomQuestionsWithAnswers(List<CategoryVM> categories, bool? questionState, int count) {
            ObservableCollection<QuestionVM> questions = new ObservableCollection<QuestionVM>();
            foreach(CategoryVM cVM in categories) {
                foreach(QuestionVM qVM in cVM.Questions) {
                    if(questionState == true) questions.Add(qVM); //alle, wenn nicht nur unbeantwortete
                    else if(qVM.IsAnswered == questionState) questions.Add(qVM); //nur wenn unbeantwortet/falsch beantwortet
                }
            }
            questions = Shuffle(questions);
            while(questions.Count > count) questions.RemoveAt(questions.Count - 1); //entferne Fragen bis gewuenschte Anzahl erreicht
            foreach(QuestionVM qVM in questions) qVM.Answers = AnswerProvider.LoadAnswers(qVM, true);
            return questions;
        }

        private static ObservableCollection<QuestionVM> Shuffle(ObservableCollection<QuestionVM> list) {
            Random r = new Random();
            List<int> done = new List<int>();
            ObservableCollection<QuestionVM> newlist = new ObservableCollection<QuestionVM>();
            int x = -1;
            for(int i = 0; i < list.Count; ++i) {
                do {
                    x = r.Next(0, list.Count);
                } while(done.Contains(x));
                done.Add(x);
                newlist.Add(list[x]);
            }
            return newlist;
        }
    }
}
