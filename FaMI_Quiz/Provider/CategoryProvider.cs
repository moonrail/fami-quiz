﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using FaMI_Quiz.ViewModel;

namespace FaMI_Quiz.Provider {
    public static class CategoryProvider {
        public static ObservableCollection<CategoryVM> LoadCategories(MainCategoryVM mainCategory, bool withChildren) {
            ObservableCollection<CategoryVM> categories = new ObservableCollection<CategoryVM>();
            List<Category> cs = App.DBAccess.GetCategoriesByMainCategory(mainCategory);
            foreach(Category c in cs) {
                CategoryVM cVM = new CategoryVM(c);
                if(withChildren) cVM.Scripts = ScriptProvider.LoadScripts(cVM);
                if(withChildren) cVM.Questions = QuestionProvider.LoadQuestions(cVM, withChildren);
                categories.Add(cVM);
            }
            return categories;
        }
    }
}
