﻿using System;
using System.Collections.Generic;
using SQLite;
using SQLiteNetExtensions.Extensions;
using Xamarin.Forms;
using FaMI_Quiz.ViewModel;

namespace FaMI_Quiz {
    public class DataAccess {
        private SQLiteConnection db;

        public DataAccess(string dbPath) {
            if(db == null) db = new SQLiteConnection(dbPath);
        }

        public void GetSettings() {
            setting sound = GetSetting(ConstVars.Setting_Sound_Name);
            if(sound != null) {
                if(sound.se_value == "0") ConstVars.Setting_Sound = false;
                else ConstVars.Setting_Sound = true;
            }
            setting pdfextern = GetSetting(ConstVars.Setting_PDFextern_Name);
            if(pdfextern != null) {
                if(pdfextern.se_value == "0") ConstVars.Setting_PDFextern = false;
                else ConstVars.Setting_PDFextern = true;
            }
            setting darktheme = GetSetting(ConstVars.Setting_DarkTheme_Name);
            if(darktheme != null) {
                if(darktheme.se_value == "0") ConstVars.Setting_DarkTheme = false;
                else ConstVars.Setting_DarkTheme = true;
            }
            setting multiplechoice_displaycount = GetSetting(ConstVars.Setting_MultipleChoice_DisplayCount_Name);
            if(multiplechoice_displaycount != null) {
                if(multiplechoice_displaycount.se_value == "0") ConstVars.Setting_MultipleChoice_DisplayCount = false;
                else ConstVars.Setting_MultipleChoice_DisplayCount = true;
            }
            setting showcorrectanswers = GetSetting(ConstVars.Setting_ShowCorrectAnswers_Name);
            if(showcorrectanswers != null) {
                if(showcorrectanswers.se_value == "0") ConstVars.Setting_ShowCorrectAnswers = false;
                else ConstVars.Setting_ShowCorrectAnswers = true;
            }
            setting dbversion = GetSetting(ConstVars.Setting_DBVersion_Name);
            if(dbversion != null) {
                if(Int32.TryParse(dbversion.se_value, out int version)) ConstVars.DBVersion_Current = version;
            }
        }

        public bool MigrateDatabaseIfNeeded() { //Migrationsprozess bei neuerer DB in der App
            if(ConstVars.DBVersion_Current < ConstVars.DBVersion_Build) { //Reminder: DBVersion_Build ist statisch eingetragen - umgeht den Schritt die mitgelieferte DB abzulegen, zu oeffnen und den Wert abzufragen, um festzustellen ob neuere DB mitgeliefert
                try {
                    List<Question> questions = db.Query<Question>("SELECT * FROM question"); //Lade Fortschritt des Benutzers aus bisheriger DB
                    db.Close();
                    string dbPath = DependencyService.Get<IAndroid>().ReplaceDatabase(ConstVars.DB_Name); // Ersetze bisherige DB
                    db = new SQLiteConnection(dbPath);
                    if(questions != null && ConstVars.DBVersion_Build != 9) { //9 war Version der DB zu Release (v1.0.0), daher sollte hier keine Fortschrittsuebernahme stattfinden
                        foreach(Question q in questions) db.Execute("UPDATE question SET q_is_answered = ? WHERE q_id = ?", q.q_is_answered, q.q_id); //Schreibe Fortschritt des Benutzers in neue DB
                    }
                    string s = string.Empty;
                    if(ConstVars.Setting_Sound) s = "1";
                    else s = "0";
                    db.Execute("UPDATE setting SET se_value = ? WHERE se_name = ?", s, ConstVars.Setting_Sound_Name);
                    if(ConstVars.Setting_PDFextern) s = "1";
                    else s = "0";
                    db.Execute("UPDATE setting SET se_value = ? WHERE se_name = ?", s, ConstVars.Setting_PDFextern_Name);
                    if(ConstVars.Setting_DarkTheme) s = "1";
                    else s = "0";
                    db.Execute("UPDATE setting SET se_value = ? WHERE se_name = ?", s, ConstVars.Setting_DarkTheme_Name);
                    if(ConstVars.Setting_MultipleChoice_DisplayCount) s = "1";
                    else s = "0";
                    db.Execute("UPDATE setting SET se_value = ? WHERE se_name = ?", s, ConstVars.Setting_MultipleChoice_DisplayCount_Name);
                    if(ConstVars.Setting_ShowCorrectAnswers) s = "1";
                    else s = "0";
                    db.Execute("UPDATE setting SET se_value = ? WHERE se_name = ?", s, ConstVars.Setting_ShowCorrectAnswers_Name);
                    setting dbversion = GetSetting(ConstVars.Setting_DBVersion_Name);
                    if(dbversion != null) {
                        if(Int32.TryParse(dbversion.se_value, out int version)) ConstVars.DBVersion_Current = version;
                    }
                    return true;
                } catch(Exception ex) {
                    ToastMessage.Long("Fehler beim Aktualisieren der Datenbank: " + ex.Message);
                }
            }
            return false;
        }

        public string GetMainCategoryAndCategoryByCategoryID(int category_id) {
            string result = "Fach\\Thema";
            List<Category> cs = db.Query<Category>("SELECT * FROM category WHERE c_id = ?", category_id);
            if(cs != null && cs.Count != 0) {
                List<MainCategory> mcs = db.Query<MainCategory>("SELECT * FROM maincategory WHERE mc_id = ?", cs[0].c_maincategory_id);
                if(mcs != null && mcs.Count != 0) result = mcs[0].mc_name + ": " + cs[0].c_name;
            }
            return result;
        }

        public void ResetProgress() {
            db.Execute("UPDATE question SET q_is_answered = 0");
        }

        public void ResetProgress(MainCategoryVM mcVM) {
            db.Execute("UPDATE question SET q_is_answered = 0 WHERE q_category_id in (SELECT c_id FROM category JOIN maincategory ON c_maincategory_id = mc_id WHERE c_maincategory_id = ?)", mcVM.ID);
        }

        public void ResetProgress(CategoryVM cVM) {
            db.Execute("UPDATE question SET q_is_answered = 0 WHERE q_category_id = " + cVM.ID);
        }

        public bool RestoreProgress(Dictionary<int,int> question_id_progress) {
            db.BeginTransaction();
            try {
                foreach(KeyValuePair<int, int> kvp in question_id_progress) db.Execute("UPDATE question SET q_is_answered = ? WHERE q_id = ?", kvp.Value, kvp.Key);
                db.Commit();
                return true;
            } catch {
                db.Rollback();
                return false;
            }
        }

        public Dictionary<int,int> GetQuestionProgress() {
            Dictionary<int, int> question_id_progress = new Dictionary<int, int>();
            List<Question> questions = db.Query<Question>("SELECT * FROM question"); //Fortschritt des Benutzers
            if(questions != null) {
                foreach(Question q in questions) question_id_progress.Add(q.q_id, q.q_is_answered);
            }
            return question_id_progress;
        }

        public List<MainCategory> GetAllMainCategories() {
            List<MainCategory> mainCategories = db.Query<MainCategory>("SELECT * FROM maincategory");
            return mainCategories;
        }

        public List<Category> GetCategoriesByMainCategory(MainCategoryVM mcVM) {
            List<Category> categories = db.Query<Category>("SELECT * FROM category WHERE c_maincategory_id = ?", mcVM.ID);
            return categories;
        }

        public List<Script> GetScriptsByCategory(CategoryVM cVM) {
            List<Script> scripts = db.Query<Script>("SELECT s_id, s_name, s_description, s_order, s_category_id, s_pdf_name FROM script WHERE s_category_id = ?", cVM.ID);
            return scripts;
        }

        public List<Question> GetQuestionsByCategory(CategoryVM cVM) {
            List<Question> questions = db.Query<Question>("SELECT * FROM question WHERE q_category_id = ?", cVM.ID);
            return questions;
        }

        public List<Answer> GetAnswersByQuestion(QuestionVM qVM) {
            List<Answer> answers = db.Query<Answer>("SELECT a_id, a_text, a_is_correct FROM answer WHERE a_question_id = ?", qVM.ID);
            return answers;
        }
        public List<Answer> GetAnswersByQuestionWithImages(QuestionVM qVM) {
            List<Answer> answers = db.Query<Answer>("SELECT a_id, a_text, a_is_correct, a_image FROM answer WHERE a_question_id = ? ORDER BY RANDOM()", qVM.ID);
            return answers;
        }

        public List<Question> GetQuestionsByCategory(List<CategoryVM> cVMs, bool? questionState, int count) {
            List<Question> questions = null;
            if(cVMs.Count > 0) {
                string mcs = "(";
                foreach(CategoryVM mcVM in cVMs) mcs += mcVM.ID.ToString() + ',';
                mcs = mcs.Remove(mcs.Length - 1) + ')';
                if(questionState == null) questions = db.Query<Question>("SELECT q_id, q_text, q_points, q_is_multiple_choice, q_is_answered, q_category_id FROM question JOIN category ON c_id in ? AND q_is_answered = 0 ORDER BY RANDOM() LIMIT ?", mcs, count);
                else if(questionState == null) questions = db.Query<Question>("SELECT q_id, q_text, q_points, q_is_multiple_choice, q_is_answered, q_category_id FROM question JOIN category ON c_id in ? AND q_is_answered = -1 ORDER BY RANDOM() LIMIT ?", mcs, count);
                else questions = db.Query<Question>("SELECT q_id, q_text, q_points, q_is_multiple_choice, q_is_answered, q_category_id FROM question JOIN category ON c_id in ? ORDER BY RANDOM() LIMIT ?", mcs, count);
            }
            return questions;
        }

        public List<Question> GetQuestionsByCategory(List<MainCategoryVM> mcVMs, bool? questionState, int count) {
            List<Question> questions = null;
            if(mcVMs.Count > 0) {
                string mcs = "(";
                foreach(MainCategoryVM mcVM in mcVMs) mcs += mcVM.ID.ToString() + ',';
                mcs = mcs.Remove(mcs.Length - 1) + ')';
                if(questionState == null) questions = db.Query<Question>("SELECT q_id, q_text, q_points, q_is_multiple_choice, q_is_answered, q_category_id FROM question JOIN category ON q_category_id = c_id JOIN maincategory ON mc_id in ? AND q_is_answered = 0 ORDER BY RANDOM() LIMIT ?", mcs, count);
                else if(questionState == false) questions = db.Query<Question>("SELECT q_id, q_text, q_points, q_is_multiple_choice, q_is_answered, q_category_id FROM question JOIN category ON q_category_id = c_id JOIN maincategory ON mc_id in ? AND q_is_answered = -1 ORDER BY RANDOM() LIMIT ?", mcs, count);
                else questions = db.Query<Question>("SELECT q_id, q_text, q_points, q_is_multiple_choice, q_is_answered, q_category_id FROM question JOIN category ON q_category_id = c_id JOIN maincategory ON mc_id in ? ORDER BY RANDOM() LIMIT ?", mcs, count);
            }
            return questions;
        }

        public int UpdateQuestion(Question q) {
            return db.Update(q);
        }

        public Answer GetAnswer(Answer a) {
            return db.GetWithChildren<Answer>(a.a_id);
        }
        public Answer GetAnswer(int answer_id) {
            return db.GetWithChildren<Answer>(answer_id);
        }

        public Script GetScript(int id) {
            List<Script> scripts = db.Query<Script>("SELECT * FROM script WHERE s_id = ?", id);
            if(scripts != null && scripts.Count != 0) return scripts[0];
            else return null;
        }

        public setting GetSetting(setting se) {
            List<setting> settings = db.Query<setting>("SELECT * FROM setting WHERE se_name = ?", se.se_name);
            if(settings != null && settings.Count != 0) return settings[0];
            else return null;
        }
        public setting GetSetting(string setting_name) {
            List<setting> settings = db.Query<setting>("SELECT * FROM setting WHERE se_name = ?", setting_name);
            if(settings != null && settings.Count != 0) return settings[0];
            else return null;
        }
        public int SaveSetting(setting se) {
            return db.Insert(se);
        }
        public int DeleteSetting(setting se) {
            return db.Delete(se);
        }
        public int UpdateSetting(setting se) {
            return db.Update(se);
        }
    }
}