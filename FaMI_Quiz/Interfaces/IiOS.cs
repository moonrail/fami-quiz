﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FaMI_Quiz {
    public interface IiOS {
        void LongAlert(string message);
        void ShortAlert(string message);
    }
}
