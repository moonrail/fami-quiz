﻿using System;

using FaMI_Quiz.View;
using Xamarin.Forms;

namespace FaMI_Quiz {
    public partial class App : Application {
        public static DataAccess DBAccess { get; private set; }

        public App() {
            InitializeComponent();
            try {
                string dbPath = null;
                if(Device.RuntimePlatform == Device.Android) dbPath = DependencyService.Get<IAndroid>().GetDBFilePath(ConstVars.DB_Name); //Object not referenced-Exception im Debugging, im Release nicht
                else if(Device.RuntimePlatform == Device.UWP) dbPath = DependencyService.Get<IUWP>().GetLocalFilePath(ConstVars.DB_Name).Result; //error? // Property UWP not Found
                DBAccess = new DataAccess(dbPath);
                DBAccess.GetSettings();
                DBAccess.MigrateDatabaseIfNeeded();
                ThemeSwitcher.Switch(true);
            } catch(Exception ex) {
                ToastMessage.Long(ex.Message);
            }
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart() {
            // Handle when app starts
        }

        protected override void OnSleep() {
            // Handle when app sleeps
        }

        protected override void OnResume() {
            // Handle when app resumes
        }
    }
}