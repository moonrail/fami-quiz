﻿using System.IO;
using System.Threading.Tasks;

namespace FaMI_Quiz {
    public interface IUWP {
        void CloseApp();
        void LongAlert(string message);
        void ShortAlert(string message);
        Task<bool> CheckIfFileExists(string fileName, int fileSize);
        string GetFilePath(string fileName);
        Task<bool> SaveFileToDisk(Stream stream, string filePath);
        Task<bool> OpenPDFExternally(string filePath);
        string GetDBFilePath(string filename);
        string ReplaceDatabase(string filename);
        void WriteFile(byte[] file, string fileName);
        void PlaySoundSuccess();
        void PlaySoundFailure();

        Task<string> GetLocalFilePath(string fileName);
    }
}
