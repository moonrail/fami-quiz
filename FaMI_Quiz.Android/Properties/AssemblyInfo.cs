﻿using System.Reflection;
using System.Runtime.InteropServices;
using Android.App;

[assembly: AssemblyTitle("FaMI Quiz")]
[assembly: AssemblyDescription("LernApp fuer FaMI-Azubis")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("JDBK-Klass IT2B (2016-2019)")]
[assembly: AssemblyProduct("FaMI.Android")]
[assembly: AssemblyCopyright("Copyright © 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.0.0.*")]
[assembly: AssemblyFileVersion("2.0.0.0")]

// Needed permissions
[assembly: UsesPermission(Android.Manifest.Permission.ReadExternalStorage)]
[assembly: UsesPermission(Android.Manifest.Permission.WriteExternalStorage)]
