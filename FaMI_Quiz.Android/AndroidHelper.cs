﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Support.V4.Content;
using Android.Widget;
using Android.Util;
using FaMI_Quiz;
using Plugin.SimpleAudioPlayer;
using System.IO;
using System.Threading.Tasks;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidHelper))]
namespace FaMI_Quiz {
    public class AndroidHelper : IAndroid {
        public void CloseApp() {
            Process.KillProcess(Process.MyPid());
        }

        public void LongAlert(string message) {
            Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message) {
            Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }

        private readonly string _rootDir = Path.Combine(Environment.ExternalStorageDirectory.Path, "FaMI");

        public async Task<bool> CheckIfFileExists(string fileName, int fileSize) {
            if(!Directory.Exists(_rootDir)) return false;
            string filePath = Path.Combine(_rootDir, fileName);
            if(File.Exists(filePath)) {
                FileInfo finfo = new FileInfo(filePath);
                if(finfo.Length == fileSize) return true;
            }
            return false;
        }

        public string GetFilePath(string fileName) { return Path.Combine(_rootDir, fileName); }

        public async Task<bool> SaveFileToDisk(Stream pdfStream, string filePath) {
            try {
                if(!Directory.Exists(_rootDir)) Directory.CreateDirectory(_rootDir);
                using(MemoryStream memoryStream = new MemoryStream()) {
                    await pdfStream.CopyToAsync(memoryStream);
                    File.WriteAllBytes(filePath, memoryStream.ToArray());
                }
                return true;
            } catch(AndroidException) {
                return false;
            }
        }

        public async Task<bool> OpenPDFExternally(string filePath) {
            Java.IO.File file = new Java.IO.File(filePath);
            file.SetReadable(true);
            Android.Net.Uri uri = FileProvider.GetUriForFile(Application.Context, Application.Context.PackageName + ".provider",file);
            Intent intent = new Intent(Intent.ActionView);
            intent.SetDataAndType(uri, "application/pdf");
            intent.SetFlags(ActivityFlags.NewTask);
            intent.AddFlags(ActivityFlags.GrantReadUriPermission);
            try {  
                Application.Context.StartActivity(intent);
                return true;
            } catch(AndroidException) {
                return false;
            }
        }

        public string GetDBFilePath(string filename) {
            Log.Debug("FaMI", "Begin DB Copy/Search");
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            Log.Debug("FaMI", "DB-Path:"+path);
            string dbPath = Path.Combine(path, filename);
            Log.Debug("FaMI", "DB-FullPath:"+dbPath);
            CopyDatabase(dbPath, filename, false);
            Log.Debug("FaMI", "DB Copied/Found");
            return dbPath;
        }

        private void CopyDatabase(string dbPath, string filename, bool replace) {
            if(!replace && File.Exists(dbPath)) return;
            else {
                using(var br = new BinaryReader(Application.Context.Assets.Open(filename))) {
                    using(var bw = new BinaryWriter(new FileStream(dbPath, FileMode.Create))) {
                        byte[] buffer = new byte[2048];
                        int length = 0;
                        while((length = br.Read(buffer, 0, buffer.Length)) > 0) {
                            bw.Write(buffer, 0, length);
                        }
                    }
                }
            }
        }

        public string ReplaceDatabase(string filename) {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string dbPath = Path.Combine(path, filename);
            CopyDatabase(dbPath, filename, true);
            return dbPath;
        }

        public void WriteFile(byte[] file, string fileName) {
            string filePath = Path.Combine(_rootDir, fileName);
            Directory.CreateDirectory(_rootDir);
            using(var br = new BinaryReader(new MemoryStream(file))) {
                using(var bw = new BinaryWriter(new FileStream(filePath, FileMode.Create))) {
                    byte[] buffer = new byte[2048];
                    int length = 0;
                    while((length = br.Read(buffer, 0, buffer.Length)) > 0) {
                        bw.Write(buffer, 0, length);
                    }
                }
            }
        }

        private ISimpleAudioPlayer player_success = null;

        public void PlaySoundSuccess() {
            if(player_success == null) {
                player_success = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                player_success.Load("success.ogg");
            }
            player_success.Play();
        }

        private ISimpleAudioPlayer player_failure = null;

        public void PlaySoundFailure() {
            if(player_failure == null) {
                player_failure = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                player_failure.Load("failure.ogg");
            }
            player_failure.Play();
        }

        public string GetVersion() {
            var context = Application.Context;
            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);
            return info.VersionName;
        }
    }
}