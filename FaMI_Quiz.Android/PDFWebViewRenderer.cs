﻿using System.Net;
using FaMI_Quiz;
using Android.Content;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(PDFWebView), typeof(FaMI_Quiz.Droid.PDFWebViewRenderer))]
namespace FaMI_Quiz.Droid {
    public class PDFWebViewRenderer : WebViewRenderer {
        public PDFWebViewRenderer(Context context) : base(context) {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e) {
            base.OnElementChanged(e);

            if(e.NewElement != null) {
                PDFWebView customWebView = Element as PDFWebView;
                Control.Settings.AllowUniversalAccessFromFileURLs = true;
                Control.LoadUrl(string.Format("file:///android_asset/pdfjs/web/viewer.html?file={0}", string.Format("file:///{0}", WebUtility.UrlEncode(customWebView.Uri))));
            }
        }

    }
}