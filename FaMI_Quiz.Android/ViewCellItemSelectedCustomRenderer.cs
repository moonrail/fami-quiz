﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using FaMI_Quiz.Droid;

//notwendig um ListView-SelectedItemColor zu aendern - Farbe ist anpassbar in Android/Ressources/Drawable/ViewCellBackground.xml

[assembly: ExportRenderer(typeof(ViewCell), typeof(ViewCellItemSelectedCustomRenderer))]
namespace FaMI_Quiz.Droid {
    public class ViewCellItemSelectedCustomRenderer : ViewCellRenderer {
        protected override Android.Views.View GetCellCore(Cell item, Android.Views.View convertView, Android.Views.ViewGroup parent, Android.Content.Context context) {
            var cell = base.GetCellCore(item, convertView, parent, context);

            cell.SetBackgroundResource(Resource.Drawable.ViewCellBackground);

            return cell;
        }
    }
}