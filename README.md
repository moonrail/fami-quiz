FaMI Quiz App
=============================

# Installation
You can download this app right away from Google Play Store:

<a href='https://play.google.com/store/apps/details?id=com.it2b.FaMIQuiz&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png'/></a>

# Description
To be clear right up front: I am not a developer, so do not expect high quality code.

This App was developed 2018 in context of an non-commercial educational project started by students visiting the vocational college [Joseph-DuMont-Berufskolleg](https://www.jdbk.de/) for their german dual education "Fachangestellte/r f�r Medien- und Informationsdienste" which translates roughly to "Specialist for media and information services".

The goal was to create a quiz app that aids future students in learning for exams or refresh their knowledge.

This app contains contents up until the intermediate exam.

I've helped those students as an external person with their project, because their ambitions were going over their heads. They've provided the questions, scripts and images.

# Why use Xamarin for a single Android App?

Because at the beginning of the project some other developers raised their hand to assist (iOS & UWP) and the students wanted to provide the app to Android, iOS and UWP, Xamarin was chosen as framework.

Suddenly there were no other developers anymore, of course only after I've already created a working prototype with Xamarin. Therefore I've abandoned iOS and UWP, but Xamarin stayed.

So this leaves an Xamarin project just for Android - not what I've would've chosen, but anyway - thats why iOS and UWP are present as sub-projects, but not anywhere tested or functional.

# Dependencies
- [Xamarin Studio Community and Visual Studio](https://www.xamarin.com)
- [SQLite.Net-PCL](https://github.com/oysteinkrog/SQLite.Net-PCL)
- [SQLiteNetExtensions](https://bitbucket.org/twincoders/sqlite-net-extensions)
- [Mozilla PDF.js](https://github.com/mozilla/pdf.js)
- [Xam.Plugin.SimpleAudioPlayer](https://github.com/adrianstevens/Xamarin-Plugins)

# Contribute
New content is not planned, as its not my profession. Content will have to be provided by someone else. Maybe you?
If any is there, I'll gather it and include it via update.
