﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

using Windows.ApplicationModel;
using Windows.Storage;
using Plugin.SimpleAudioPlayer;

namespace FaMI_Quiz.UWP {
    public class UWPHelper : IUWP {
        public async Task<string> GetLocalFilePath(string filename) {
            string dbPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, filename);
            bool isExisting = false;
            try {
                StorageFile storage = await ApplicationData.Current.LocalFolder.GetFileAsync(filename);
                isExisting = true;
            } catch(Exception) {
                isExisting = false;
            }
            if(!isExisting) {
                StorageFile databaseFile = await Package.Current.InstalledLocation.GetFileAsync(filename);
                await databaseFile.CopyAsync(ApplicationData.Current.LocalFolder, filename, NameCollisionOption.ReplaceExisting);
            }
            return dbPath;
        }

        public void CloseApp() {
            Windows.UI.Xaml.Application.Current.Exit();
        }

        public void LongAlert(string message) {
            //Toast.MakeText(Application.Context, message, ToastLength.Long).Show();
        }

        public void ShortAlert(string message) {
            //Toast.MakeText(Application.Context, message, ToastLength.Short).Show();
        }

        private readonly string _rootDir = Path.Combine(ApplicationData.Current.LocalFolder.Path, "FaMI");

        public async Task<bool> CheckIfFileExists(string fileName, int fileSize) {
            if(!Directory.Exists(_rootDir)) return false;
            string filePath = Path.Combine(_rootDir, fileName);
            if(File.Exists(filePath)) {
                FileInfo finfo = new FileInfo(filePath);
                if(finfo.Length == fileSize) return true;
            }
            return false;
        }

        public string GetFilePath(string fileName) { return Path.Combine(_rootDir, fileName); }

        public async Task<bool> SaveFileToDisk(Stream pdfStream, string filePath) {
            try {
                if(!Directory.Exists(_rootDir)) Directory.CreateDirectory(_rootDir);
                using(MemoryStream memoryStream = new MemoryStream()) {
                    await pdfStream.CopyToAsync(memoryStream);
                    File.WriteAllBytes(filePath, memoryStream.ToArray());
                }
                return true;
            } catch {
                LongAlert("PDF konnte nicht gespeichert werden");
                return false;
            }
        }

        public async Task<bool> OpenPDFExternally(string filePath) {
            //Java.IO.File file = new Java.IO.File(filePath);
            //Android.Net.Uri uri = Android.Net.Uri.FromFile(file);
            //Intent intent = new Intent(Intent.ActionView);
            //intent.SetDataAndType(uri, "application/pdf");
            //intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask);
            try {
                //Application.Context.StartActivity(intent);
                Process.Start(filePath);
                return true;
            } catch {
                LongAlert("Keine externe PDF-App vorhanden");
                return false;
            }
        }

        public string GetDBFilePath(string filename) {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string dbPath = Path.Combine(path, filename);
            CopyDatabase(dbPath, filename, false);
            return dbPath;
        }

        private void CopyDatabase(string dbPath, string filename, bool replace) {
            //if(!replace && File.Exists(dbPath)) return;
            //else {
            //    using(var br = new BinaryReader(Application.Context.Assets.Open(filename))) {
            //        using(var bw = new BinaryWriter(new FileStream(dbPath, FileMode.Create))) {
            //            byte[] buffer = new byte[2048];
            //            int length = 0;
            //            while((length = br.Read(buffer, 0, buffer.Length)) > 0) {
            //                bw.Write(buffer, 0, length);
            //            }
            //        }
            //    }
            //}
        }

        public string ReplaceDatabase(string filename) {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string dbPath = Path.Combine(path, filename);
            CopyDatabase(dbPath, filename, true);
            return dbPath;
        }

        public void WriteFile(byte[] file, string fileName) {
            string filePath = Path.Combine(_rootDir, fileName);
            using(var br = new BinaryReader(new MemoryStream(file))) {
                using(var bw = new BinaryWriter(new FileStream(filePath, FileMode.Create))) {
                    byte[] buffer = new byte[2048];
                    int length = 0;
                    while((length = br.Read(buffer, 0, buffer.Length)) > 0) {
                        bw.Write(buffer, 0, length);
                    }
                }
            }
        }

        private ISimpleAudioPlayer player_success = null;

        public void PlaySoundSuccess() {
            if(player_success == null) {
                player_success = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                player_success.Load("success.ogg");
            }
            player_success.Play();
        }

        private ISimpleAudioPlayer player_failure = null;

        public void PlaySoundFailure() {
            if(player_failure == null) {
                player_failure = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
                player_failure.Load("failure.ogg");
            }
            player_failure.Play();
        }
    }
}
